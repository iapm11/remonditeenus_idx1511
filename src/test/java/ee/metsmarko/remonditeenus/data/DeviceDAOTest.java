package ee.metsmarko.remonditeenus.data;

import ee.metsmarko.remonditeenus.model.Device;
import ee.metsmarko.remonditeenus.model.DeviceType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by katre on 9.11.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class DeviceDAOTest {

    @Autowired
    private DeviceDAO deviceDAO;

    @Autowired
    private DataSource dataSource;

    @Test(expected=NullPointerException.class)
    public void testNullPointerException() {
        Device device = new Device();
        device.setName("hyperboloid");
        deviceDAO.findDevice(device, new Integer[0]);
    }

    @Test
    public void testFindDevice() {
        List<Device> devices = new ArrayList<>();
        devices.add(getCompleteTestDevice());

        Assert.assertEquals(devices, deviceDAO.findDevice(getPartialTestDevice(), new Integer[0]));
    }

    @Test
    public void testSetClientList() throws Exception {
        Connection connection = dataSource.getConnection();
        Array clientList = connection.createArrayOf("numeric", new Integer[]{1, 2});
        Device device = getPartialTestDevice();
        String sql = getLongSql();

        PreparedStatement expectedStatement = connection.prepareStatement(
                sql.replace("%placeholder%", "'{\"1\",\"2\"}'"));
        PreparedStatement actualStatement = connection.prepareStatement(
                sql.replace("%placeholder%", "?"));

        deviceDAO.setClientList(actualStatement, clientList);

        deviceDAO.setParameters(device, expectedStatement);
        deviceDAO.setParameters(device, actualStatement);

        ResultSet expectedResultSet = expectedStatement.executeQuery();
        ResultSet actualResultSet = actualStatement.executeQuery();

        Assert.assertEquals(buildDeviceList(expectedResultSet), buildDeviceList(actualResultSet));
    }

    @Test
    public void testSetParameters() throws Exception {
        Connection connection = dataSource.getConnection();

        PreparedStatement statement1 = connection.prepareStatement(getSqlWithReplacements());
        PreparedStatement statement2 = connection.prepareStatement(getSqlWithoutReplacements());

        deviceDAO.setParameters(getPartialTestDevice(), statement2);

        ResultSet rs1 = statement1.executeQuery();
        ResultSet rs2 = statement2.executeQuery();

        Assert.assertEquals(buildDeviceList(rs1), buildDeviceList(rs2));
    }

    private Device getCompleteTestDevice() {
        Device d = new Device();
        DeviceType dt = new DeviceType();
        d.setDevice(1);
        d.setName("Insener Garini hyperboloid");
        d.setReg_no("G327347273");
        d.setDescription(null);
        d.setModel("GAR1444");
        d.setManufacturer("Garin Industries");
        dt.setDevice_type(5);
        d.setDt(dt);
        return d;
    }

    private Device getPartialTestDevice() {
        Device d = new Device();
        DeviceType dt = new DeviceType();
        d.setName("hyperboloid");
        dt.setDevice_type(5);
        d.setDt(dt);
        return d;
    }

    private String getSqlWithoutReplacements() {
        return "SELECT * FROM device WHERE (? is NULL OR device_type_fk=?) AND " +
                " (? is NULL OR (UPPER(name) LIKE UPPER(?))) AND" +
                " (? is NULL OR (UPPER(reg_no) LIKE UPPER(?))) AND" +
                " (? is NULL OR (UPPER(description) LIKE UPPER(?))) AND" +
                " (? is NULL OR (UPPER(model) LIKE UPPER(?))) AND" +
                " (? is NULL OR (UPPER(manufacturer) LIKE UPPER(?)))";
    }

    private String getSqlWithReplacements() {
        return "SELECT * FROM device WHERE (5 is NULL OR device_type_fk=5) AND " +
                " ('hyperboloid' is NULL OR (UPPER(name) LIKE UPPER('%hyperboloid%')))";
    }

    private String getLongSql() {
        return "SELECT * FROM device WHERE (? is NULL OR device_type_fk=?) AND " +
                " (? is NULL OR (UPPER(name) LIKE UPPER(?))) AND" +
                " (? is NULL OR (UPPER(reg_no) LIKE UPPER(?))) AND" +
                " (? is NULL OR (UPPER(description) LIKE UPPER(?))) AND" +
                " (? is NULL OR (UPPER(model) LIKE UPPER(?))) AND" +
                " (? is NULL OR (UPPER(manufacturer) LIKE UPPER(?)))" +
                " AND (device in (SELECT device.device FROM device INNER JOIN service_device ON device_fk=device " +
                "INNER JOIN service_order ON service_order_fk = service_order " +
                "INNER JOIN service_request ON service_request_fk = service_request " +
                "WHERE customer_fk = ANY (%placeholder%)))";
    }

    private List<Device> buildDeviceList(ResultSet rs) throws SQLException {
        List<Device> devices = new ArrayList<>();
        while (rs.next()) {
            Device d = new Device();
            d.setDevice(rs.getInt("device"));
            devices.add(d);
        }
        return devices;
    }

}
