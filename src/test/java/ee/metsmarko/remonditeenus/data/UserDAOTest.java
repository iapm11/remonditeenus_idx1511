package ee.metsmarko.remonditeenus.data;

import ee.metsmarko.remonditeenus.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * Created by Karmo on 010 10 11 2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class UserDAOTest {

    @Autowired
    private UserDAO userDAO;

    @Test
    public void testGetUserByUsername() throws Exception {
        User actual = userDAO.getUserByUsername("kaarel");
        Assert.assertEquals(4, actual.getId().intValue());
        Assert.assertEquals("kaarel", actual.getName());
        Assert.assertEquals(4, actual.getTypeId());
        Assert.assertEquals("klient", actual.getType().getName());
        Assert.assertEquals(1, actual.getSubject_fk().intValue());
        Assert.assertEquals("123", actual.getPassword());
    }

    @Test
    public void testGetUserByUsername_whenNotFound_returnsNull()
            throws Exception {
        Assert.assertNull(userDAO.getUserByUsername("hjkl"));
    }
}
