package ee.metsmarko.remonditeenus.data;

import ee.metsmarko.remonditeenus.model.Client;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * Created by Karmo on 010 10 11 2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class ClientDAOTest {

    @Autowired
    private ClientDAO clientDAO;

    @Test
    public void testSearchPeople() throws Exception {
        List<Client> clients = clientDAO.searchPeople("Klient");
        Assert.assertEquals(1, clients.size());

        Client c = clients.get(0);
        Assert.assertEquals(1, c.getId());
        Assert.assertEquals("Kaarel Klient", c.getName());
    }

    @Test
    public void testSearchPeople_findsMultipleClients() throws Exception {
        List<Client> clients = clientDAO.searchPeople("k");
        Assert.assertEquals(2, clients.size());
    }

    @Test
    public void testSearchEnterprises() throws Exception {
        List<Client> clients = clientDAO.searchEnterprises("Torupood OY");
        Assert.assertEquals(1, clients.size());

        Client c = clients.get(0);
        Assert.assertEquals(3, c.getId());
        Assert.assertEquals("Torupood OY", c.getName());
    }

    @Test
    public void testGetClientByUserAccountId() throws Exception {
        Client c = clientDAO.getClientByUserAccountId(4);
        Assert.assertEquals(1, c.getId());
        Assert.assertEquals("Kaarel Klient", c.getName());
    }

}
