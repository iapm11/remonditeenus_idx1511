package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.repository.GeneralRepository;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.mockito.Mockito.*;
import static ee.metsmarko.remonditeenus.service.EntityStubService.EntityStub;

/**
 * Created by mihkelk on 4.10.2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class GeneralServiceTest {

    public static final String EXPECTED_STRING = "TEST STRING";
    public static final int EXPECTED_INTEGER = 1;

    @Autowired
    private EntityStubService entityStubService;
    private GeneralService generalServiceTest;

    private GeneralRepository generalRepositoryMock;
    private EntityStub entity;

    @Before
    public void setUp() {
        generalServiceTest = spy(entityStubService);
        generalRepositoryMock = mock(GeneralRepository.class);
        generalServiceTest.generalRepository = generalRepositoryMock;
        entity = new EntityStub();
    }

    @Test
    public void testFindById() {
        generalServiceTest.findById(EXPECTED_INTEGER);
        verify(generalRepositoryMock, times(1)).findById(EXPECTED_INTEGER,
                EntityStub.class);

    }

    @Test
    @SuppressWarnings("unchecked")
    public void testAdd() {
        generalServiceTest.add(entity);
        verify(generalRepositoryMock, times(1)).add(entity);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testMerge() {
        generalServiceTest.merge(entity);
        verify(generalRepositoryMock, times(1)).merge(entity);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testRemove() {
        when(generalRepositoryMock.merge(any())).thenReturn(entity);
        generalServiceTest.remove(entity);
        verify(generalRepositoryMock, times(1)).remove(entity);
    }

    @Test
    public void testFindAll() {
        generalServiceTest.findAll();
        verify(generalRepositoryMock, times(1)).findAll(EntityStub.class);

    }

    @Test
    public void testFindByQuery() {
        generalServiceTest.findByQuery(EXPECTED_STRING);
        verify(generalRepositoryMock, times(1)).findByQuery(EXPECTED_STRING);

    }
}