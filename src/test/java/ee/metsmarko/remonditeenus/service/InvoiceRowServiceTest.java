package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Invoice;
import ee.metsmarko.remonditeenus.model.Order;
import ee.metsmarko.remonditeenus.model.Part;
import org.junit.Assert;
import org.junit.Test;
import ee.metsmarko.remonditeenus.model.*;
import org.apache.logging.log4j.core.util.*;
import org.junit.*;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by Salome on 10.11.2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class InvoiceRowServiceTest {

    @Autowired
    private InvoiceRowServiceSpy invoiceRowService;
    private Invoice invoice = new Invoice();
    private Order order = new Order();
    private Part part = new Part();
    private Set<Part> partList = new HashSet<>();

    @Before
    public void setUp() throws Exception {
        order.setId(1);
        invoice.setOrder(order);
    }

    @Test
    public void addRows() {
        invoice.setDescription("lalalala");
        order.setId(1);
        partList.add(part);
        order.setParts(partList);
        invoice.setOrder(order);
        invoiceRowService.addRows(invoice);
        Assert.assertEquals(6, invoiceRowService.invoiceRows.size());
    }

}
