package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Device;
import ee.metsmarko.remonditeenus.model.DeviceType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Salome on 09.11.2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class DeviceServiceTest {

    Device device = new Device();
    DeviceType deviceType = new DeviceType();

    @Autowired
    private DeviceService deviceService;

    @Test
    public void findDevice() {
        device.setName("hyperboloid");
        deviceType.setDevice_type(5);
        device.setDt(deviceType);
        List<Device> devices = deviceService.findDevice(device);
        Assert.assertEquals(1, devices.size());
        Assert.assertEquals("Insener Garini hyperboloid", devices.get(0).getName());
    }

    @Test(expected = NullPointerException.class)
    public void findDeviceWithoutType() {
        device.setName("hyperboloid");
        deviceService.findDevice(device);
    }

    @Test
    public void findDeviceById() {
        Assert.assertEquals("Insener Garini hyperboloid", deviceService.findDeviceById(1).getName());
    }


}
