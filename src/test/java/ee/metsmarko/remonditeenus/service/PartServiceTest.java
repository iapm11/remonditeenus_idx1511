package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Part;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by caroliinalaantee on 12/11/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class PartServiceTest {

    @Autowired
    private PartService partService;

    @Test
    public void findByOrderAndDevice() {
        List<Part> parts = partService.findByOrderAndDevice(1, 2);
        assertEquals(parts.size(), 2);
        assertEquals(parts.get(0).getPart_name(), "laserkahuri aku AK74");
        assertEquals(parts.get(1).getPart_name(), "puldi aku");
    }
}
