package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Action;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Karmo on 029 29 10 2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class ActionServiceTest {

    @Autowired
    private ActionService actionService;

    @Test
    public void testFindByOrderAndDevice() throws Exception {
        List<Action> actions = actionService.findByOrderAndDevice(1, 1);
        Assert.assertEquals(1, actions.size());
        Action action = actions.get(0);
        Assert.assertEquals(1, action.getService_order_fk().intValue());
        Assert.assertEquals(1, action.getDevice().getDevice());
    }
}
