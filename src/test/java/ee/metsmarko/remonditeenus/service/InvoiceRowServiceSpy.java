package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.InvoiceRow;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Salome on 10.11.2015.
 */

@Service
public class InvoiceRowServiceSpy extends InvoiceRowService {

    List<InvoiceRow> invoiceRows = new ArrayList<>();

    @Override
    public void add(InvoiceRow item) {
        invoiceRows.add(item);
    }
}
