package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Note;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karmo on 19.11.15.
 */

@Service
public class NoteServiceSpy extends NoteService {
    List<Note> notes = new ArrayList<>();

    @Override
    public void add(Note item) {
        notes.add(item);
    }
}
