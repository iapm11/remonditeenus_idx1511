package ee.metsmarko.remonditeenus.service;

import org.springframework.stereotype.Service;

/**
 * Created by karmo on 22.11.15.
 */

@Service
class EntityStubService extends GeneralService<EntityStubService.EntityStub> {
    static class EntityStub {
    }
    public EntityStubService() {
        super(EntityStub.class);
    }
}
