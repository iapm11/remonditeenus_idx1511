package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * Created by caroliinalaantee on 12/11/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void getUserByName() {
        User existingUser = userService.getUserByName("juhan");
        Assert.assertNotNull(existingUser);
        User missingUser = userService.getUserByName("jaanus123!");
        Assert.assertNull(missingUser);
    }
}
