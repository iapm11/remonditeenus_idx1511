package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.enums.NoteAuthorTypeEnum;
import ee.metsmarko.remonditeenus.model.Customer;
import ee.metsmarko.remonditeenus.model.Employee;
import ee.metsmarko.remonditeenus.model.Note;
import ee.metsmarko.remonditeenus.model.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by karmo on 17.11.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class NoteServiceTest {

    private UserService userService;
    User user = new User();
    @Autowired
    NoteServiceSpy noteServiceSpy;
    ArgumentCaptor<Note> notes = ArgumentCaptor.forClass(Note.class);;

    @Before
    public void setUp() {
        userService = mock(UserService.class);
        noteServiceSpy.userService = userService;
        noteServiceSpy.employeeService.userService = userService;
        when(userService.getLoggedInUser()).thenReturn(user);
        when(userService.isEmployee()).thenReturn(Boolean.TRUE);
    }

    @After
    public void tearDown() throws Exception {
        noteServiceSpy.notes.clear();
    }

    @Test
    public void testAddClientNote() throws Exception {
        user.setId(4);
        noteServiceSpy.addClientNote(new Note());
        Assert.assertEquals(1, noteServiceSpy.notes.size());

        Note actual = noteServiceSpy.notes.get(0);
        Assert.assertNotNull(actual.getCreated());
        Assert.assertEquals(NoteAuthorTypeEnum.KLIENT.getId(),
                actual.getNote_author_type());

        assertCustomerId(1, actual);
    }

    private void assertCustomerId(int customerId, Note actual) {
        Customer expected = new Customer();
        expected.setId(customerId);
        Assert.assertEquals(expected, actual.getCustomer());
    }

    @Test
    public void testAddEmployeeNote() throws Exception {
        int subjectFk = 3;
        user.setId(3);
        user.setSubject_fk(subjectFk);
        noteServiceSpy.addEmployeeNote(new Note());
        Assert.assertEquals(1, noteServiceSpy.notes.size());

        Note actual = noteServiceSpy.notes.get(0);
        Assert.assertNotNull(actual.getCreated());
        Assert.assertEquals(NoteAuthorTypeEnum.TOOTAJA.getId(),
                actual.getNote_author_type());

        Employee expected = noteServiceSpy.employeeService.findById(subjectFk);
        Assert.assertEquals(expected, actual.getEmployee());
    }

}