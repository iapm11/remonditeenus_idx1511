package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Client;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Karmo on 029 29 10 2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class ClientServiceTest {
    @Autowired
    private ClientService clientService;

    @Test
     public void testSearchClient_findsCustomer() throws Exception {
        List<Client> clients = clientService.searchClient("klient");
        Assert.assertEquals(1, clients.size());
        Assert.assertEquals("Kaarel Klient", clients.get(0).getName());
    }

    @Test
    public void testSearchClient_findsEnterprise() throws Exception {
        List<Client> clients = clientService.searchClient("torupood");
        Assert.assertEquals(1, clients.size());
        Assert.assertEquals("Torupood OY", clients.get(0).getName());
    }

    @Test
    public void testSearchClient_findsMultipleItems() throws Exception {
        List<Client> clients = clientService.searchClient("t");
        Assert.assertEquals(2, clients.size());
    }

    @Test
    public void testFindById_findsCustomer() throws Exception {
        Client client = clientService.findById(1);
        Assert.assertEquals("Kaarel Klient", client.getName());
    }

    @Test
    public void testFindById_findsEnterprise() throws Exception {
        Client client = clientService.findById(3);
        Assert.assertEquals("Torupood OY", client.getName());
    }

    @Test
    public void testGetClientByUserAccountId() throws Exception {
        Client client = clientService.getClientByUserAccountId(4);
        Assert.assertEquals("Kaarel Klient", client.getName());
    }
}
