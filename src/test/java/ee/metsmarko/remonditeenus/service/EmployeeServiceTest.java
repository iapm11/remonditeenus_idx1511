package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.enums.SubjectTypeEnum;
import ee.metsmarko.remonditeenus.model.Employee;
import ee.metsmarko.remonditeenus.model.SubjectType;
import ee.metsmarko.remonditeenus.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Karmo on 009 9 11 2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@ActiveProfiles({"test"})
@Transactional
public class EmployeeServiceTest {

    private UserService userService;
    User user = new User();
    @Autowired
    private EmployeeService employeeService;

    @Before
    public void setUp() {
        userService = mock(UserService.class);
        user.setType(new SubjectType());

        employeeService.userService = userService;
        when(userService.getLoggedInUser()).thenReturn(user);
        when(userService.isEmployee()).thenCallRealMethod();
    }

    @Test
    public void testGetLoggedInEmployee() throws Exception {
        user.setSubject_fk(3); // tanel
        user.getType().setId(SubjectTypeEnum.TOOTAJA.getId());

        Employee employee = employeeService.getLoggedInEmployee();
        Assert.assertNotNull(employee);
        Assert.assertEquals(3, employee.getId().intValue());
    }

    @Test
    public void testGetLoggedInEmployee_whenCustomerIsLoggedIn_returnsNull()
            throws Exception {
        user.setSubject_fk(4); // kaarel
        user.getType().setId(SubjectTypeEnum.KLIENT.getId());

        Assert.assertNull(employeeService.getLoggedInEmployee());
    }
}
