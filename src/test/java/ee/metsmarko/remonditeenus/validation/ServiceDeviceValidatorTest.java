package ee.metsmarko.remonditeenus.validation;

import ee.metsmarko.remonditeenus.model.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by karmo on 3.06.15.
 */
public class ServiceDeviceValidatorTest {

    ServiceDevice serviceDevice;
    ServiceDeviceValidator validator;

    @Before
    public void setUp() throws Exception {
        serviceDevice = createServiceDevice();
        validator = new ServiceDeviceValidator(serviceDevice);
    }

    private ServiceDevice createServiceDevice() {
        ServiceDevice sd = new ServiceDevice();
        ServiceDeviceStatus status = new ServiceDeviceStatus();
        status.setId(1);
        sd.setStatus(status);
        sd.setActions(new ArrayList<Action>());
        sd.setParts(new ArrayList<Part>());
        return sd;
    }

    @Test
    public void testValidate() throws Exception {
        assertTrue(validator.validateUpdate().get("status") == null);
        addIncompleteAction();
        assertTrue(validator.validateUpdate().get("status") == null);
        serviceDevice.getStatus().setId(2);
        assertTrue(validator.validateUpdate().get("status") != null);
        serviceDevice.getActions().get(0).getStatus().setId(2);
        assertTrue(validator.validateUpdate().get("status") == null);
    }

    private void addIncompleteAction() {
        Action action = new Action();
        ActionStatus actionStatus = new ActionStatus();
        actionStatus.setId(1);
        action.setStatus(actionStatus);
        serviceDevice.getActions().add(action);
    }

    @Test
    public void testValidateDelete_hasActions() throws Exception {
        assertTrue(validator.validateDelete().get("remove") == null);
        addIncompleteAction();
        assertTrue(validator.validateDelete().get("remove") != null);
    }

    @Test
    public void testValidateDelete_hasParts() throws Exception {
        assertTrue(validator.validateDelete().get("remove") == null);
        addPart();
        assertTrue(validator.validateDelete().get("remove") != null);
    }

    private void addPart() {
        Part part = new Part();
        serviceDevice.getParts().add(part);
    }
}