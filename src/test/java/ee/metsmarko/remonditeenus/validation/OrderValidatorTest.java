package ee.metsmarko.remonditeenus.validation;

import ee.metsmarko.remonditeenus.model.*;
import org.junit.Before;
import org.junit.Test;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by karmo on 1.06.15.
 */
public class OrderValidatorTest {

    Order updated = createOrder();
    OrderValidator validator;

    @Before
    public void setUp() throws Exception {
        validator = new OrderValidator(updated);
    }

    private Order createOrder() {
        Order o = new Order();
        OrderStatus os = new OrderStatus();
        os.setId(1);
        o.setStatus(os);
        return o;
    }

    @Test
    public void testValidateStatusChange() throws Exception {
        setOrderStatusToPriceMade();
        assertTrue(validator.validateUpdate().get("status") == null);
        addIncompleteServiceDevice();
        assertTrue(validator.validateUpdate().get("status") != null);
    }

    private void addIncompleteServiceDevice() {
        ServiceDevice sd = new ServiceDevice();
        ServiceDeviceStatus status = new ServiceDeviceStatus();
        status.setId(1);
        sd.setStatus(status);
        updated.getServiceDevices().add(sd);
    }

    private void setOrderStatusToPriceMade() {
        updated.getStatus().setId(3);
    }

    @Test
    public void testValidatePartSerialNoAndCount() throws Exception {
        Part part = new Part();
        updated.getParts().add(part);
        part.setPart_count(17);
        assertTrue(validator.validateUpdate().get("parts.serial_no") == null);
        part.setSerial_no("TX-100");
        assertTrue(validator.validateUpdate().get("parts.serial_no") != null);
    }

    @Test
    public void testValidateDelete() throws Exception {
        assertTrue(validator.validateDelete().get("status") == null);
        updated.getStatus().setId(4);
        assertTrue(validator.validateDelete().get("status") != null);
    }
}