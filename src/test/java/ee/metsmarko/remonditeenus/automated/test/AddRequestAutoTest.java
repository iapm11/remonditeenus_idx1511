package ee.metsmarko.remonditeenus.automated.test;

import ee.metsmarko.remonditeenus.service.ServiceRequestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.by;
import static com.codeborne.selenide.Selenide.$;
import static junit.framework.TestCase.assertTrue;

/**
 *
 * Created by mihkelk on 22.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:**/applicationContext.xml"})
@Transactional
public class AddRequestAutoTest extends LoginTootajaTest {

    @Autowired
    private ServiceRequestService srs;

    @Test
    public void testAddRequest() throws InterruptedException {

        int sizeBefore = srs.findAll().size();

        $("#lisa_poordumine").click();
        $(by("onclick", "openSearch()")).should(appear);
        $(by("onclick", "openSearch()")).click();

        $("#userfinder").val("k");

        $(by("onclick", "findClient()")).click();

        $(".clients").should(appear);
        $("#klient_1").click();

        $("#customer_name").shouldHave(value("Kaarel Klient"));
        $("#service_desc_by_customer").val("12345678901234511");
        $("#service_desc_by_employee").val("12345678901234511");
        $("#add_request").click();

        $("#userfinder").should(disappear);

        int sizeAfter = srs.findAll().size();

        assertTrue(sizeAfter == ++sizeBefore);
    }
}
