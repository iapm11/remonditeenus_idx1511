package ee.metsmarko.remonditeenus.automated.test;

import org.junit.Before;

/**
 *
 * Created by caroliinalaantee on 29/10/15.
 */
public abstract class LoginTootajaTest implements LoginTest {
    @Before
    public void setUp() {
        LoginTest.super.loginTest();
    }

    public String getUrl() {
        return "http://localhost:8080/rteenus/login";
    }

    public String getUsername() {
        return "tanel";
    }

    public String getPassword() {
        return "123";
    }

    public String getElement() {
        return "menu";
    }
}
