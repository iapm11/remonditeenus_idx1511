package ee.metsmarko.remonditeenus.automated.test;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by caroliinalaantee on 29/10/15.
 */
public interface LoginTest {

    default void loginTest() {
        open(getUrl());
        $(By.name("username")).val(getUsername());
        $(By.name("password")).val(getPassword()).pressEnter();
        $(By.className(getElement())).shouldBe(Condition.present);
    }

    String getUrl();
    String getUsername();
    String getPassword();
    String getElement();
}
