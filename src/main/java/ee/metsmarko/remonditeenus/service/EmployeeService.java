package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Employee;
import ee.metsmarko.remonditeenus.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by karmo on 30.05.15.
 */
@Component
public class EmployeeService extends GeneralService<Employee> {

    @Autowired
    UserService userService;

    public EmployeeService() {
        super(Employee.class);
    }

    public Employee getLoggedInEmployee() {
        User user = userService.getLoggedInUser();
        if (user != null && userService.isEmployee())
            return findById(user.getSubject_fk());
        return null;
    }

}
