package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.repository.Param;
import ee.metsmarko.remonditeenus.repository.IGeneralRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 *
 * Created by Karmo on 021 21 05 2015.
 * Modified by Mihkel in here.
 */
public abstract class GeneralService<T> {

    @Autowired
    IGeneralRepository<T> generalRepository;

    private Class<T> genericType;

    public GeneralService(Class<T> genericType) {
        this.genericType = genericType;
    }

    @SuppressWarnings("unchecked")
    public T findById(int id) {
        return generalRepository.findById(id, genericType);
    }

    public void add(T item) {
        generalRepository.add(item);
    }

    public T merge(T item) {
        return generalRepository.merge(item);
    }

    public void remove(T item) {
        generalRepository.remove(item);
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        return generalRepository.findAll(genericType);
    }

    @SuppressWarnings("unchecked")
    public List<T> findByQuery(String query, Param... params) {
        return generalRepository.findByQuery(query, params);
    }
}
