package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.ServiceDeviceStatus;
import org.springframework.stereotype.Service;

/**
 * Created by karmo on 3.06.15.
 */
@Service
public class ServiceDeviceStatusService extends GeneralService<ServiceDeviceStatus> {
    public ServiceDeviceStatusService() {
        super(ServiceDeviceStatus.class);
    }
}
