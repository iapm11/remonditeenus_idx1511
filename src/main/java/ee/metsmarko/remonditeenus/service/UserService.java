package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.data.UserDAO;
import ee.metsmarko.remonditeenus.enums.SubjectTypeEnum;
import ee.metsmarko.remonditeenus.model.User;
import ee.metsmarko.remonditeenus.security.UserDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * Created by Marko on 21.05.2015.
 */
@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public User getUserByName(String name){
        return userDAO.getUserByUsername(name);
    }

    public User getLoggedInUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            UserDetail userDetails = (UserDetail) principal;
            return userDetails.getUser();
        }
        return null;
    }

    public boolean isEmployee() {
        return getLoggedInUser().getTypeId() == SubjectTypeEnum.TOOTAJA.getId();
    }
}
