package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.repository.Param;
import ee.metsmarko.remonditeenus.model.Action;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by karmo on 2.06.15.
 */
@Service
public class ActionService extends GeneralService<Action> {
    public ActionService() {
        super(Action.class);
    }

    public List<Action> findByOrderAndDevice(int orderId, int deviceId) {
        String query = "SELECT a FROM Action a WHERE a.service_order_fk = :orderId" +
                " AND a.device.device = :deviceId";

        return findByQuery(query, new Param("orderId", orderId),
                new Param("deviceId", deviceId));
    }
}
