package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Customer;
import org.springframework.stereotype.Service;

/**
 * Created by karmo on 26.05.15.
 */
@Service
public class CustomerService extends GeneralService<Customer> {

    public CustomerService() {
        super(Customer.class);
    }

}
