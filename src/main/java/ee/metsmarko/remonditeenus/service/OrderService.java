package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.enums.OrderStatusEnum;
import ee.metsmarko.remonditeenus.repository.Param;
import ee.metsmarko.remonditeenus.model.*;
import ee.metsmarko.remonditeenus.validation.OrderValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by karmo on 24.05.15.
 * Modified by Mihkel on 29.10.2015 - refactored for protsessid ained
 */
@Service
public class OrderService extends GeneralService<Order>
        implements IOrderByIdService {

    @Autowired
    private ClientService clientService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private UserService userService;

    public OrderService() {
        super(Order.class);
    }

    public Collection<Order> searchOrders(OrderQuery queryParams) {
        List<Client> clients = clientService.searchClient(queryParams.getClientName());
        if (clients.isEmpty())
            return new ArrayList<>();
        List<Integer> clientIds = extractClientIds(clients);
        Set<Order> found = new HashSet<>(searchByParams(queryParams, clientIds));
        setCustomerNames(clients, found);
        return found;
    }

    private List<Integer> extractClientIds(List<Client> clients) {
        return clients.stream()
                .map(Client::getId)
                .collect(Collectors.toList());
    }

    private List<Order> searchByParams(OrderQuery queryParams, List<Integer> clientIds) {
        String query = "SELECT o FROM Order o JOIN o.serviceDevices sd" +
                " WHERE (o.service_request.customer_fk IN (:clientIds))" +
                " AND (sd.device.name LIKE ('%' || :deviceName || '%'))" +
                " AND (sd.device.model LIKE '%' || :deviceModel || '%')" +
                " AND (o.note IS NULL OR o.note LIKE '%' || :note || '%')";
        return findByQuery(query,
                new Param("clientIds", clientIds),
                new Param("deviceName", queryParams.getDeviceName()),
                new Param("deviceModel", queryParams.getDeviceModel()),
                new Param("note", queryParams.getNote())
        );
    }

    private void setCustomerNames(List<Client> clients, Set<Order> found) {
        found.forEach(order -> clients.stream()
                .filter(customer -> isCustomerPartOfOrder(order, customer))
                .forEach(customer -> order.setCustomer_name(customer.getName())));
    }

    private boolean isCustomerPartOfOrder(Order order, Client customer) {
        return order.getService_request().getCustomer_fk() == customer.getId();
    }

    public Order getOrder(int id) {
        Order order = findById(id);
        if (order != null) {
            setCustomerName(order);
        }
        return order;
    }

    private void setCustomerName(Order order) {
        int id = order.getService_request().getCustomer_fk();
        Client client = clientService.findById(id);
        if (client != null) {
            order.setCustomer_name(client.getName());
        }
    }

    public PostResponse update(Order order) {
        Order old = findById(order.getId());
        Map<String, String> errors = new OrderValidator(order).validateUpdate();
        if (!errors.isEmpty()) {
            return new PostResponse("Tellimuses leidub vigaseid andmeid", errors);
        }
        updateFieldsBasedOnStatus(order, old);
        return new PostResponse();
    }

    private void updateFieldsBasedOnStatus(Order order, Order oldOrder) {
        boolean statusChanged = !oldOrder.getStatus().equals(order.getStatus());
        if (hasNoPrice(oldOrder)) {
            updateOrder(order, statusChanged);
        } else {
            updateStatusOnly(order, oldOrder, statusChanged);
        }
    }

    private boolean hasNoPrice(Order oldOrder) {
        return OrderStatusEnum.VASTU_VOETUD.equals(oldOrder.getStatus())
                || OrderStatusEnum.VALMIS.equals(oldOrder.getStatus());
    }

    private void updateStatusOnly(Order order, Order oldOrder, boolean statusChanged) {
        oldOrder.setStatus(order.getStatus());
        updateOrder(oldOrder, statusChanged);
    }

    private void updateOrder(Order order, boolean statusChanged) {
        Employee employee = employeeService.getLoggedInEmployee();
        if (statusChanged) {
            order.setStatus_changed_by(employee);
            order.setStatus_changed(new Date());
        }
        order.setUpdated_by(employee);
        order.setUpdated(new Date());

        setCreatedByToNewActions(order, employee);
        setCreatedByToNewParts(order, employee);
        setCreatedByToNewNotes(order);

        merge(order);
    }

    private void setCreatedByToNewActions(Order order, Employee employee) {
        order.getActions().stream().filter(action -> action.getId() == null).forEach(action -> {
            action.setCreated(new Date());
            action.setCreated_by(employee);
        });
    }

    private void setCreatedByToNewParts(Order order, Employee employee) {
        order.getParts().stream().filter(part -> part.getId() == null).forEach(p -> {
            p.setCreated(new Date());
            p.setCreated_by(employee);
        });
    }

    private void setCreatedByToNewNotes(Order order) {
        order.getNotes().stream().filter(note -> note.getId() == null).forEach(note -> {
            note.setCreated(new Date());
            User u = userService.getLoggedInUser();
            if (userService.isEmployee()) {
                note.setNote_author_type(2);
                note.setEmployee(employeeService.getLoggedInEmployee());
            } else {
                note.setNote_author_type(1);
                note.setCustomer(customerService.findById(u.getSubject_fk()));
            }
        });
    }

    /**
     * Adds a new service order.
     *
     * @param request_id Id of the service request from what order is made.
     * @return Id of the new order.
     */
    public Integer add(Integer request_id) {
        Order order = new Order();
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setService_request(request_id);
        OrderStatus orderStatus = new OrderStatus();
        orderStatus.setId(1);
        order.setService_request(serviceRequest);
        order.setCreated(new Date());
        order.setCreated_by(employeeService.getLoggedInEmployee());
        order.setStatus(orderStatus);
        super.add(order);
        return order.getId();
    }

    public List<Order> getLoggedInCientsOrders() {
        User user = userService.getLoggedInUser();
        Client client = clientService.getClientByUserAccountId(user.getId());
        String query = "SELECT o FROM Order o " +
                "WHERE o.service_request.customer_fk = :clientId";
        return findByQuery(query, new Param("clientId", client.getId()));
    }

    public PostResponse delete(int orderId) {
        Order order = findById(orderId);

        Map<String, String> errors = new OrderValidator(order).validateDelete();
        if (!errors.isEmpty()) {
            return new PostResponse("Viga kustutamisel", errors);
        }

        remove(order);
        return new PostResponse();
    }
}
