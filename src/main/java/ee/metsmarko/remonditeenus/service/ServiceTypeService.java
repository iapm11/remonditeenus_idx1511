package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.ServiceType;
import org.springframework.stereotype.Service;

/**
 * Created by karmo on 29.05.15.
 */
@Service
public class ServiceTypeService extends GeneralService<ServiceType> {

    public ServiceTypeService() {
        super(ServiceType.class);
    }

}
