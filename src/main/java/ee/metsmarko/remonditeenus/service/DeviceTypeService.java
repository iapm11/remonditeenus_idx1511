package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.DeviceType;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * Created by Marko on 26.05.2015.
 */
@Service
public class DeviceTypeService extends GeneralService<DeviceType> {

    public DeviceTypeService() {
        super(DeviceType.class);
    }

    public List<DeviceType> getDeviceTypes() {
        return findAll();
    }
}
