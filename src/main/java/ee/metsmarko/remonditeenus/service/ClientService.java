package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.data.ClientDAO;
import ee.metsmarko.remonditeenus.model.Client;
import ee.metsmarko.remonditeenus.model.Customer;
import ee.metsmarko.remonditeenus.model.Enterprise;
import ee.metsmarko.remonditeenus.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by karmo on 23.05.15.
 */
@Service
public class ClientService {

    @Autowired
    private ClientDAO clientDAO;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private PersonService personService;
    @Autowired
    private EnterpriseService enterpriseService;

    public List<Client> searchClient(String name) {
        List<Client> result = clientDAO.searchPeople(name);
        result.addAll(clientDAO.searchEnterprises(name));
        return result;
    }

    public Client findById(int id) {
        Customer customer = customerService.findById(id);
        if (customer.getType().getId() == 1)
            return getFromPerson(id, customer);
        else
            return getFromEnterprise(id, customer);

    }

    private Client getFromPerson(int id, Customer customer) {
        Person p = personService.findById(customer.getSubject_fk());
        if (p != null)
            return new Client(id, p);
        return null;
    }

    private Client getFromEnterprise(int id, Customer customer) {
        Enterprise e = enterpriseService.findById(customer.getSubject_fk());
        if (e != null)
            return new Client(id, e);
        return null;
    }

    public Client getClientByUserAccountId(int id){
        return clientDAO.getClientByUserAccountId(id);
    }

}
