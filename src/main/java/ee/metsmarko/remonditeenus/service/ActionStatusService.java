package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.ActionStatus;
import org.springframework.stereotype.Service;

/**
 *
 * Created by karmo on 28.05.15.
 */
@Service
public class ActionStatusService extends  GeneralService<ActionStatus> {

    public ActionStatusService() {
        super(ActionStatus.class);
    }

}
