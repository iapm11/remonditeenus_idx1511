package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.repository.Param;
import ee.metsmarko.remonditeenus.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * Created by Marko on 4.06.2015.
 */
@Service
public class InvoiceRowService extends GeneralService<InvoiceRow> {

    @Autowired
    private OrderService orderService;

    public InvoiceRowService() {
        super(InvoiceRow.class);
    }

    public void addRows(Invoice invoice) {
        Order order = orderService.getOrder(invoice.getOrder().getId());
        for (Part part : order.getParts()) {
            add(createPartRow(part, invoice));
        }
        for (Action action : order.getActions()) {
            add(createActionRow(action, invoice));
        }
    }

    private InvoiceRow createPartRow(Part part, Invoice invoice) {
        InvoiceRow invoiceRow = new InvoiceRow();
        invoiceRow.setInvoice(invoice);
        invoiceRow.setServicePart(part);
        invoiceRow.setAction_part_description(part.getPart_name());
        invoiceRow.setPrice_total(part.getPart_price() * part.getPart_count());
        invoiceRow.setUnit_price(part.getPart_price());
        invoiceRow.setAmount(part.getPart_count());
        invoiceRow.setUnit_type("tk.");
        invoiceRow.setInvoice_row_type(1);
        return invoiceRow;
    }

    private InvoiceRow createActionRow(Action a, Invoice invoice) {
        InvoiceRow invoiceRow = new InvoiceRow();
        invoiceRow.setInvoice(invoice);
        invoiceRow.setServiceAction(a);
        invoiceRow.setAction_part_description(a.getAction_description());
        invoiceRow.setPrice_total(a.getService_amount() * a.getPrice());
        invoiceRow.setUnit_price(a.getPrice());
        invoiceRow.setAmount(a.getService_amount());
        invoiceRow.setUnit_type(a.getType().getType_name());
        invoiceRow.setInvoice_row_type(2);
        return invoiceRow;
    }

    public List<InvoiceRow> getRows(Integer id) {
        String query = "SELECT i FROM InvoiceRow i WHERE i.invoice.invoice = :id";
        return findByQuery(query, new Param("id", id));
    }
}
