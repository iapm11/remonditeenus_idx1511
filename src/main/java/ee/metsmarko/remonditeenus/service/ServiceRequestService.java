package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.data.RequestDAO;
import ee.metsmarko.remonditeenus.enums.ServiceRequestStatusEnum;
import ee.metsmarko.remonditeenus.repository.Param;
import ee.metsmarko.remonditeenus.model.ServiceRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Marko on 20.05.2015.
 * Modified by Salome 09.11.2015
 */
@Service
public class ServiceRequestService extends GeneralService<ServiceRequest> {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private RequestDAO requestDAO;

    public ServiceRequestService() {
        super(ServiceRequest.class);
    }

    private List<ServiceRequest> getRequest(Integer userId, ServiceRequestStatusEnum status) {
        String query = " SELECT s FROM ServiceRequest s WHERE s.customer_fk = :userId " +
                "AND s.service_request_status_type_fk = :status";
        return findByQuery(query, new Param("userId", userId), new Param("status", status.getId()));
}

    private List<ServiceRequest> getAllRequests(ServiceRequestStatusEnum status) {
        String query = " SELECT s FROM ServiceRequest s WHERE " +
                "s.service_request_status_type_fk = :status";
        return findByQuery(query, new Param("status", status.getId()));
    }

    public List<ServiceRequest> getAllRegisteredRequests() {
        return getAllRequests(ServiceRequestStatusEnum.REGISTREERITUD);
    }

    public List<ServiceRequest> getRegisteredRequests(Integer userId) {
        return getRequest(userId, ServiceRequestStatusEnum.REGISTREERITUD);
    }

    public List<ServiceRequest> getCancelledRequests(Integer userId) {
        return getRequest(userId,ServiceRequestStatusEnum.TAGASI_LYKATUD);
    }

    public List<ServiceRequest> getConfirmedRequests(Integer userId) {
        return getRequest(userId, ServiceRequestStatusEnum.TELLIMUS_TEHTUD);
    }

    public ServiceRequest getRequestById(Integer id) {
        return findById(id);
    }

    public void add(ServiceRequest serviceRequest) {
        serviceRequest.setCreated(new Date());
        serviceRequest.setService_request_status_type_fk(1);
        serviceRequest.setCreated_by(employeeService.getLoggedInEmployee().getId());
        super.add(serviceRequest);
    }

    public void cancelRequest(Integer id) {
        requestDAO.cancelRequest(id);
    }

    public void acceptRequest(Integer id) {
        requestDAO.acceptRequest(id);
    }
}
