package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Person;
import org.springframework.stereotype.Service;

/**
 * Created by karmo on 27.05.15.
 */
@Service
public class PersonService extends GeneralService<Person> {

    public PersonService() {
        super(Person.class);
    }

}
