package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.repository.Param;
import ee.metsmarko.remonditeenus.model.Invoice;
import ee.metsmarko.remonditeenus.model.InvoiceStatus;
import ee.metsmarko.remonditeenus.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Marko on 4.06.2015.
 * Modified by Salome 09.11.2015.
 */
@Service
public class InvoiceService extends GeneralService<Invoice> {

    @Autowired
    private IOrderByIdService orderByIdService;

    public InvoiceService() {
        super(Invoice.class);
    }

    public void add(Invoice invoice) {
        Order order = orderByIdService.getOrder(invoice.getOrder().getId());
        invoice.setPrice_total(order.getPrice_total());
        InvoiceStatus invoiceStatus = new InvoiceStatus();
        invoiceStatus.setInvoice_status_type(1);
        invoice.setInvoiceStatus(invoiceStatus);
        invoice.setReference_number(order.getId().toString());
        super.add(invoice);
    }

    public void confirmInvoice(Integer invoiceId) {
        Invoice invoice = super.findById(invoiceId);
        InvoiceStatus invoiceStatus = new InvoiceStatus();
        invoiceStatus.setInvoice_status_type(2);
        invoice.setInvoiceStatus(invoiceStatus);
        invoice.setInvoice_date(new Date());
        super.merge(invoice);
    }

    public void saveInvoice(Invoice invoice) {
        if (invoiceIsNotNull(invoice)) {
            super.merge(invoice);
        } else {
            this.add(invoice);
        }
    }

    private boolean invoiceIsNotNull(Invoice invoice) {
        return invoice.getInvoice() != null && super.findById(invoice.getInvoice()) != null;
    }

    public Invoice getInvoiceByOrderId(Integer id) {
        String query = " SELECT i FROM Invoice i WHERE i.order.id = :id";
        List<Invoice> result = findByQuery(query, new Param("id", id));
        if (result == null || result.size() == 0) {
            return null;
        }
        return result.get(0);
    }
}
