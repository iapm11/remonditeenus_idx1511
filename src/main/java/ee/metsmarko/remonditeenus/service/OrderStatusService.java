package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.OrderStatus;
import org.springframework.stereotype.Service;

/**
 * Created by karmo on 27.05.15.
 */
@Service
public class OrderStatusService extends GeneralService<OrderStatus> {

    public OrderStatusService() {
        super(OrderStatus.class);
    }

}
