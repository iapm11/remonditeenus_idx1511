package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.data.DeviceDAO;
import ee.metsmarko.remonditeenus.model.Client;
import ee.metsmarko.remonditeenus.model.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Marko on 26.05.2015.
 */
@Service
public class DeviceService extends GeneralService<Device> {

    @Autowired
    private DeviceDAO deviceDAO;

    @Autowired
    private ClientService clientService;

    public DeviceService() {
        super(Device.class);
    }

    public List<Device> findDevice(Device device) {
        Integer[] clientIds;
        if (device.getCustomer_name() != null) {
            List<Client> clients = clientService.searchClient(device.getCustomer_name());
            clientIds = retrieveClientIds(clients);
        } else {
            clientIds = new Integer[0];
        }
        return deviceDAO.findDevice(device, clientIds);
    }

    private Integer[] retrieveClientIds(List<Client> clients) {
        Integer[] clientIds = new Integer[clients.size()];
        for (int i = 0; i < clientIds.length; i++) {
            clientIds[i] = clients.get(i).getId();
        }
        return clientIds;
    }

    public Device findDeviceById(Integer id) {
        return findById(id);
    }
}
