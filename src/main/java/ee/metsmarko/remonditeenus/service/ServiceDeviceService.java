package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.enums.ServiceDeviceStatusEnum;
import ee.metsmarko.remonditeenus.repository.Param;
import ee.metsmarko.remonditeenus.model.Device;
import ee.metsmarko.remonditeenus.model.PostResponse;
import ee.metsmarko.remonditeenus.model.ServiceDevice;
import ee.metsmarko.remonditeenus.model.ServiceDeviceStatus;
import ee.metsmarko.remonditeenus.validation.ServiceDeviceValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Marko on 27.05.2015.
 * Modified by Mihkel on 29.09.2015
 */
@Service
public class ServiceDeviceService extends GeneralService<ServiceDevice>{

    @Autowired
    private ActionService actionService;
    @Autowired
    private PartService partService;

    public ServiceDeviceService(){
        super(ServiceDevice.class);
    }

    public void add(ServiceDevice serviceDevice){
        Date date = new Date();
        ServiceDeviceStatus serviceDeviceStatus = new ServiceDeviceStatus();
        serviceDeviceStatus.setId(ServiceDeviceStatusEnum.VASTU_VOETUD.getId());
        serviceDevice.setFrom_store(date);
        serviceDevice.setStatus_changed(date);
        serviceDevice.setStatus(serviceDeviceStatus);
        super.add(serviceDevice);
    }

    /**
     * Adds devices to a service order
     * @param devices IDs of the devices to be added.
     * @param orderId ID of the order.
     */
    public void add(Integer[] devices, Integer orderId) {
        for(Integer i : devices){
            ServiceDevice sd = new ServiceDevice();
            Device d = new Device();
            d.setDevice(i);
            sd.setService_order_fk(orderId);
            sd.setDevice(d);
            this.add(sd);
        }
    }

    public ServiceDevice getServiceDevice(int id) {
        ServiceDevice sd = findById(id);
        sd.setActions(actionService.findByOrderAndDevice(
                sd.getService_order_fk(), sd.getDevice().getDevice()));
        sd.setParts(partService.findByOrderAndDevice(
                sd.getService_order_fk(), sd.getDevice().getDevice()));
        return sd;
    }

    public List<ServiceDevice> getServiceDeviceList(int orderId) {
        String query = "SELECT sd FROM ServiceDevice sd" +
                " WHERE service_order_fk = :orderId";
        return findByQuery(query, new Param("orderId", orderId));
    }

    public PostResponse update(ServiceDevice updated) {
        Map<String, String> errors = new ServiceDeviceValidator(updated).validateUpdate();
        if (!errors.isEmpty()) {
            return new PostResponse("Salvestamine ebaõnnestus", errors);
        }
        updateFields(updated);
        return new PostResponse();
    }

    private void updateFields(ServiceDevice updated) {
        ServiceDevice old = findById(updated.getId());
        if (old.getStatus().equals(updated.getStatus())) {
            updated.setStatus_changed(new Date());
        }
        if (ServiceDeviceStatusEnum.SEADE_KLIENDILE_TAGASTATUD.equals(updated.getStatus())
                && !ServiceDeviceStatusEnum.SEADE_KLIENDILE_TAGASTATUD.equals(old.getStatus())) {
            updated.setTo_store(new Date());
        }
        merge(updated);
    }

    public PostResponse delete(int id) {
        ServiceDevice sd = getServiceDevice(id);
        Map<String, String> errors = new ServiceDeviceValidator(sd).validateDelete();
        if (!errors.isEmpty()) {
            return new PostResponse("Viga seadme eemaldamisel", errors);
        }
        remove(sd);
        return new PostResponse();
    }
}
