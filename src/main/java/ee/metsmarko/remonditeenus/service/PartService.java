package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.repository.Param;
import ee.metsmarko.remonditeenus.model.Part;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by karmo on 3.06.15.
 */
@Service
public class PartService extends GeneralService<Part> {
    public PartService() {
        super(Part.class);
    }

    public List<Part> findByOrderAndDevice(int orderId, int deviceId) {
        String query = "SELECT a FROM Part a WHERE a.service_order_fk = :orderId" +
                " AND a.device.device = :deviceId";

        return findByQuery(query, new Param("orderId", orderId),
                new Param("deviceId", deviceId));
    }
}
