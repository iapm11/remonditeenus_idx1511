package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Order;

/**
 * Created by Karmo on 007 7 06 2015.
 */
public interface IOrderByIdService {
    Order getOrder(int id);
}
