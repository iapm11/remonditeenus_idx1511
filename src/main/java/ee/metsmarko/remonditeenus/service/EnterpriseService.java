package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.model.Enterprise;
import org.springframework.stereotype.Service;

/**
 * Created by karmo on 27.05.15.
 */
@Service
public class EnterpriseService extends GeneralService<Enterprise> {

    public EnterpriseService() {
        super(Enterprise.class);
    }

}
