package ee.metsmarko.remonditeenus.service;

import ee.metsmarko.remonditeenus.enums.NoteAuthorTypeEnum;
import ee.metsmarko.remonditeenus.model.Customer;
import ee.metsmarko.remonditeenus.model.Employee;
import ee.metsmarko.remonditeenus.model.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by Marko on 31.05.2015.
 * Modified by Salome on 09.11.2015.
 */
@Service
public class NoteService extends GeneralService<Note>{

    @Autowired
    private ClientService clientService;

    @Autowired
    UserService userService;

    @Autowired
    EmployeeService employeeService;

    public NoteService(){
        super(Note.class);
    }

    public void addClientNote(Note note){
        Customer customer = constructNoteCustomer();
        note.setCustomer(customer);
        note.setNote_author_type(NoteAuthorTypeEnum.KLIENT.getId());
        note.setCreated(new Date());
        add(note);
    }

    private Customer constructNoteCustomer() {
        Customer customer = new Customer();
        customer.setId(getClient());
        return customer;
    }

    private Integer getClient() {
        return clientService.getClientByUserAccountId(userService.getLoggedInUser().getId()).getId();
    }

    public void addEmployeeNote(Note note) {
        Employee emp = employeeService.getLoggedInEmployee();
        note.setEmployee(emp);
        note.setNote_author_type(NoteAuthorTypeEnum.TOOTAJA.getId());
        note.setCreated(new Date());
        add(note);
    }
}
