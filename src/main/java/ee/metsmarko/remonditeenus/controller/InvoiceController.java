package ee.metsmarko.remonditeenus.controller;

import ee.metsmarko.remonditeenus.model.Invoice;
import ee.metsmarko.remonditeenus.service.InvoiceRowService;
import ee.metsmarko.remonditeenus.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Marko on 4.06.2015.
 */
@Controller
@RequestMapping("/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private InvoiceRowService invoiceRowService;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody void saveInvoice(@RequestBody Invoice invoice){
        Integer givenId = invoice.getInvoice();
        invoiceService.saveInvoice(invoice);
        Integer savedId = invoice.getInvoice();
        if (givenId == null || !savedId.equals(givenId)) {
            invoiceRowService.addRows(invoice);
        }
    }

    @RequestMapping(value = "/confirm/{invoiceId}", method = RequestMethod.POST)
    public @ResponseBody void confirmInvoice(@PathVariable Integer invoiceId){
        invoiceService.confirmInvoice(invoiceId);
    }

    @RequestMapping(value = "/{orderId}", method = RequestMethod.GET)
    public @ResponseBody Invoice getOrdersInvoice(@PathVariable Integer orderId){
        Invoice invoice = invoiceService.getInvoiceByOrderId(orderId);
        if(invoice == null){
            return null;
        } else {
            invoice.setInvoiceRows(invoiceRowService.getRows(invoice.getInvoice()));
            return invoice;
        }
    }

}
