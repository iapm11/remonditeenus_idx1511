package ee.metsmarko.remonditeenus.controller;

import ee.metsmarko.remonditeenus.model.Note;
import ee.metsmarko.remonditeenus.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Marko on 31.05.2015.
 */
@Controller
@RequestMapping("/note")
public class NoteController {

    @Autowired
    private NoteService noteService;

    @RequestMapping(value = "/client/add", method = RequestMethod.POST)
    public @ResponseBody void addClientNote(@RequestBody Note note){
        noteService.addClientNote(note);
    }

    @RequestMapping(value = "/employee/add", method = RequestMethod.POST)
    public @ResponseBody void addEmployeeNote(@RequestBody Note note){
        noteService.addEmployeeNote(note);
    }

}
