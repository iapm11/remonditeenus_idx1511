package ee.metsmarko.remonditeenus.controller;

import ee.metsmarko.remonditeenus.exception.UnauthorizedException;
import ee.metsmarko.remonditeenus.service.ServiceRequestService;
import ee.metsmarko.remonditeenus.model.ServiceRequest;
import ee.metsmarko.remonditeenus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Marko on 20.05.2015.
 */
@Controller
@RequestMapping("/request")
public class RequestController {

    @Autowired
    private UserService userService;

    @Autowired
    private ServiceRequestService serviceRequestService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String getAddRequestView(Model model) {
        model.addAttribute("serviceRequest", new ServiceRequest());
        return "addrequest";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addRequest(@Valid ServiceRequest serviceRequest, BindingResult result, Model model) {
        if (!userService.isEmployee()) {
            throw new UnauthorizedException();
        }
        if (!result.hasErrors()) {
            serviceRequestService.add(serviceRequest);
            model.addAttribute("message", "Pöördumine lisatud");
        }
        model.addAttribute("serviceRequest", serviceRequest);
        return "addrequest";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getRequests() {
        return "requests";
    }

    @RequestMapping(value = "/registered/{userId}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<ServiceRequest> getRegisteredRequests(@PathVariable Integer userId) {
        return serviceRequestService.getRegisteredRequests(userId);
    }

    @RequestMapping(value = "/registered/all", method = RequestMethod.GET)
    public
    @ResponseBody
    List<ServiceRequest> getAllRegisteredRequests() {
        return serviceRequestService.getAllRegisteredRequests();
    }

    @RequestMapping(value = "/cancelled/{userId}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<ServiceRequest> getCancelledRequests(@PathVariable Integer userId) {
        return serviceRequestService.getCancelledRequests(userId);
    }

    @RequestMapping(value = "/confirmed/{userId}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<ServiceRequest> getConfirmedRequests(@PathVariable Integer userId) {
        return serviceRequestService.getConfirmedRequests(userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getRequestByIf(@PathVariable Integer id, Model model) {
        model.addAttribute("request", serviceRequestService.findById(id));
        return "request";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public
    @ResponseBody
    void cancelRequest(@PathVariable Integer id) {
        serviceRequestService.cancelRequest(id);
    }

}
