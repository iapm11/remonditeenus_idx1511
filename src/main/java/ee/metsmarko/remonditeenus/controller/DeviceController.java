package ee.metsmarko.remonditeenus.controller;

import ee.metsmarko.remonditeenus.model.Device;
import ee.metsmarko.remonditeenus.model.DeviceType;
import ee.metsmarko.remonditeenus.service.DeviceService;
import ee.metsmarko.remonditeenus.service.DeviceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Marko on 26.05.2015.
 */
@Controller
@RequestMapping("/device")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DeviceTypeService deviceTypeService;

    @RequestMapping(value = "/find", method = RequestMethod.POST)
    public
    @ResponseBody
    List<Device> findDevices(@RequestBody Device device) {
        return deviceService.findDevice(device);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Device> addDevice(@RequestBody @Valid Device device, BindingResult result) {
        if (result.hasErrors()) {
            return new ResponseEntity<Device>(HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
            deviceService.add(device);
            return new ResponseEntity<Device>(device, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/findById/{id}")
    public
    @ResponseBody
    Device findDeviceById(@PathVariable Integer id) {
        return deviceService.findDeviceById(id);
    }

    @RequestMapping("/types")
    public
    @ResponseBody
    List<DeviceType> getDeviceTypes() {
        return deviceTypeService.getDeviceTypes();
    }

}
