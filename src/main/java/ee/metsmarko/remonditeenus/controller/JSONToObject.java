package ee.metsmarko.remonditeenus.controller;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * Created by karmo on 3.06.15.
 */
public class JSONToObject {

    public static<T> T parse(String json, Class<T> type) {
        Gson gson = buildGsonUnderstandingUnixTimestamp();
        return gson.fromJson(json, type);
    }

    private static Gson buildGsonUnderstandingUnixTimestamp() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class,
                new JsonDeserializer<Date>() {
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        });
        return builder.create();
    }
}
