package ee.metsmarko.remonditeenus.controller;

import ee.metsmarko.remonditeenus.model.PostResponse;
import ee.metsmarko.remonditeenus.model.ServiceDevice;
import ee.metsmarko.remonditeenus.model.ServiceDeviceStatus;
import ee.metsmarko.remonditeenus.service.ServiceDeviceService;
import ee.metsmarko.remonditeenus.service.ServiceDeviceStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Marko on 27.05.2015.
 */
@Controller
@RequestMapping("/servicedevice")
public class ServiceDeviceController {

    @Autowired
    private ServiceDeviceService serviceDeviceService;
    @Autowired
    private ServiceDeviceStatusService serviceDeviceStatusService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody void addServiceDevice(@RequestBody ServiceDevice serviceDevice){
        serviceDeviceService.add(serviceDevice);
    }

    @RequestMapping(value="id/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ServiceDevice getServiceDevice(@PathVariable int id) {
        return serviceDeviceService.getServiceDevice(id);
    }

    @RequestMapping(value="orderid/{orderId}", method = RequestMethod.GET)
    public @ResponseBody
    List<ServiceDevice> getServiceDeviceList(@PathVariable int orderId) {
        return serviceDeviceService.getServiceDeviceList(orderId);
    }

    @RequestMapping(value="statuslist", method = RequestMethod.GET)
    public @ResponseBody
    List<ServiceDeviceStatus> getOrderStatusList() {
        return serviceDeviceStatusService.findAll();
    }

    @RequestMapping(value="update", method = RequestMethod.POST)
    public @ResponseBody
    PostResponse update(@RequestParam(value = "servicedevice") String sdJson) {
        ServiceDevice sd = JSONToObject.parse(sdJson, ServiceDevice.class);
        return serviceDeviceService.update(sd);
    }

    @RequestMapping(value="remove/{id}", method = RequestMethod.POST)
    public @ResponseBody
    PostResponse remove(@PathVariable(value = "id") int id) {
        return serviceDeviceService.delete(id);
    }

}
