package ee.metsmarko.remonditeenus.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by karmo on 6.06.15.
 */
@ControllerAdvice
public class ExceptionController {
    final static Logger logger = LogManager.getLogger(ExceptionController.class);

    @ExceptionHandler(value = Exception.class)
    public ModelAndView handleError(Exception e)
    {
        logger.catching(e);
        return null;
    }

}
