package ee.metsmarko.remonditeenus.controller;

import ee.metsmarko.remonditeenus.model.Client;
import ee.metsmarko.remonditeenus.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Marko on 23.05.2015.
 */
@Controller
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/findbyname/{name}", method = RequestMethod.GET)
    public @ResponseBody
    List<Client> getClientByName(@PathVariable String name){
        return clientService.searchClient(name);
    }

}
