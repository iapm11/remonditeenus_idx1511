package ee.metsmarko.remonditeenus.controller;

import ee.metsmarko.remonditeenus.model.*;
import ee.metsmarko.remonditeenus.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

/**
 *
 * Created by karmo on 24.05.15.
 */
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderStatusService orderStatusService;
    @Autowired
    private ActionStatusService actionStatusService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private ServiceDeviceService serviceDeviceService;
    @Autowired
    private ServiceRequestService serviceRequestService;
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private InvoiceRowService invoiceRowService;

    @RequestMapping(method = RequestMethod.GET)
    public String getOrderView() {
        return "order";
    }

    @RequestMapping(value = "searchorder", method = RequestMethod.POST)
    public @ResponseBody
    Collection<Order> searchOrders(@RequestBody OrderQuery query) {
        return orderService.searchOrders(query);
    }

    @RequestMapping(value="id/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Order getOrder(@PathVariable Integer id) {
        return orderService.getOrder(id);
    }

    @RequestMapping(value="statuslist", method = RequestMethod.GET)
    public @ResponseBody
    List<OrderStatus> getOrderStatusList() {
        return orderStatusService.findAll();
    }

    @RequestMapping(value="action/statuslist", method = RequestMethod.GET)
    public @ResponseBody
    List<ActionStatus> getActionStatusList() {
        return actionStatusService.findAll();
    }

    @RequestMapping(value="action/typelist", method = RequestMethod.GET)
    public @ResponseBody
    List<ServiceType> getActionTypeList() {
        return serviceTypeService.findAll();
    }

    @RequestMapping(value="update", method = RequestMethod.POST)
    public @ResponseBody
    PostResponse updateOrder(@RequestParam(value = "order") String orderJson) {
        Order order = JSONToObject.parse(orderJson, Order.class);
        return orderService.update(order);
    }

    @RequestMapping(value="delete", method = RequestMethod.POST)
    public @ResponseBody
    PostResponse updateOrder(@RequestParam(value = "orderid") int orderId) {
        return orderService.delete(orderId);
    }

    @RequestMapping(value="add/{request_id}", method = RequestMethod.POST)
    public @ResponseBody Integer submitOrder(@RequestBody Integer[] devices, @PathVariable Integer request_id){
        Integer orderId = orderService.add(request_id);
        serviceDeviceService.add(devices, orderId);
        serviceRequestService.acceptRequest(request_id);
        return orderId;
    }

    @RequestMapping(value="adddevices/{orderId}", method = RequestMethod.POST)
    public @ResponseBody
    PostResponse addDevices(@PathVariable int orderId, @RequestBody Integer[] devices) {
        serviceDeviceService.add(devices, orderId);
        return new PostResponse();
    }

    @RequestMapping(value = "my", method = RequestMethod.GET)
    public String getClientsOrders(Model model){
        List<Order> orders = orderService.getLoggedInCientsOrders();
        model.addAttribute("orders", orders);
        return "clientorders";
    }

    @RequestMapping(value = "my/{id}", method = RequestMethod.GET)
    public String getLoggedInClientsOrder(@PathVariable Integer id, Model model){
        Order order = orderService.getOrder(id);
        model.addAttribute("order", order);
        Invoice invoice = invoiceService.getInvoiceByOrderId(id);
        if(invoice != null){
            invoice.setInvoiceRows(invoiceRowService.getRows(invoice.getInvoice()));
            model.addAttribute("invoice", invoice);
        }
        return "clientorder";
    }
}
