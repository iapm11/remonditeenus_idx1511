package ee.metsmarko.remonditeenus.data;

import ee.metsmarko.remonditeenus.model.Client;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by karmo on 24.05.15.
 * Modified by Katre on 29.10.15.
 */
@Service
public class ClientDAO extends DataAccessObject {

    public List<Client> searchPeople(String lastName){
        String sql = "SELECT customer, first_name, last_name FROM customer" +
                " INNER JOIN person p ON subject_fk = person" +
                " WHERE UPPER(p.last_name) LIKE UPPER(?);";
        return executePersonQuery(sql, lastName);
    }

    @SneakyThrows
    private List<Client> executePersonQuery(String sql, String lastName) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "%" + lastName + "%");
            return buildPersonList(statement.executeQuery());
        }
    }

    @SneakyThrows
    private List<Client> buildPersonList(ResultSet rs) {
        try {
            return readPeopleFromResultSet(rs);
        } finally {
            close(rs);
        }
    }

    private List<Client> readPeopleFromResultSet(ResultSet rs) throws SQLException {
        List<Client> list = new ArrayList<>();
        while (rs.next()) {
            list.add(buildPerson(rs));
        }
        return list;
    }

    private Client buildPerson(ResultSet rs) throws SQLException {
        int id = rs.getInt("customer");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        return new Client(id, firstName + " " + lastName);
    }

    @SneakyThrows
    public List<Client> searchEnterprises(String name){
        String sql = "SELECT customer, full_name FROM customer" +
                " INNER JOIN enterprise e ON subject_fk = enterprise" +
                " WHERE UPPER(e.full_name) LIKE UPPER(?);";
        return executeEnterpriseQuery(sql, name);
    }

    private List<Client> executeEnterpriseQuery(String sql, String name) throws SQLException {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "%" + name + "%");
            return buildEnterpriseList(statement.executeQuery());
        }
    }

    @SneakyThrows
    private List<Client> buildEnterpriseList(ResultSet rs) {
        List<Client> list = new ArrayList<>();
        try {
            while (rs.next()) {
                int id = rs.getInt("customer");
                String name = rs.getString("full_name");
                list.add(new Client(id, name));
            }
            return list;
        } finally {
            close(rs);
        }
    }

    public Client getClientByUserAccountId(int id) {
        String sql = "SELECT customer, first_name, last_name FROM customer c INNER JOIN" +
                " user_account ua ON ua.subject_fk = c.customer INNER JOIN " +
                "person p ON c.subject_fk = p.person WHERE user_account  = ?";
        return prepareAndExecuteGetClientStatement(id, sql);
    }

    @SneakyThrows
    private Client prepareAndExecuteGetClientStatement(int id, String sql) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            return executeAndBuildPerson(statement);
        }
    }

    @SneakyThrows
    private Client executeAndBuildPerson(PreparedStatement statement) {
        try (ResultSet rs = statement.executeQuery()) {
            if (!rs.next()) {
                return null;
            }
            return buildPerson(rs);
        }
    }

}
