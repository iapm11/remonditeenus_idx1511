package ee.metsmarko.remonditeenus.data;

import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Marko on 25.05.2015.
 * Modified by Caroliina on 29.10.2015.
 */
@Component
public class RequestDAO extends DataAccessObject{

    public void cancelRequest(int id){
        ResultSet rs = null;
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = getConnection();
            String sql = "SELECT f_cancel_request(?);";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            close(rs, statement, connection);
        }
    }

    public void acceptRequest(int id){
        ResultSet rs = null;
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = getConnection();
            String sql = "SELECT f_accept_request(?);";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            close(rs, statement, connection);
        }
    }
}
