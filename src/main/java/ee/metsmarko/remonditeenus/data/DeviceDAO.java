package ee.metsmarko.remonditeenus.data;

import ee.metsmarko.remonditeenus.model.Device;
import ee.metsmarko.remonditeenus.model.DeviceType;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marko on 26.05.2015.
 * Modified by Caroliina on 29.10.2015.
 * Modified by Katre on 10.11.2015.
 */
@Component
public class DeviceDAO extends DataAccessObject {

    private final static String DEVICE = "device";
    private final static String DEVICE_TYPE_FK = "device_type_fk";
    private final static String NAME = "name";
    private final static String REG_NO = "reg_no";
    private final static String DESCRIPTION = "description";
    private final static String MODEL = "model";
    private final static String MANUFACTURER = "manufacturer";


    public List<Device> findDevice(Device device, Integer[] clientIds) {
        ResultSet rs = null;
        PreparedStatement statement = null;
        Connection connection = null;
        Array clientList = null;
        try {
            connection = getConnection();
            String sql = "SELECT * FROM device WHERE (? is NULL OR device_type_fk=?) AND " +
                    " (? is NULL OR (UPPER(name) LIKE UPPER(?))) AND" +
                    " (? is NULL OR (UPPER(reg_no) LIKE UPPER(?))) AND" +
                    " (? is NULL OR (UPPER(description) LIKE UPPER(?))) AND" +
                    " (? is NULL OR (UPPER(model) LIKE UPPER(?))) AND" +
                    " (? is NULL OR (UPPER(manufacturer) LIKE UPPER(?)))";

            if (device.getCustomer_name() != null) {
                sql += " AND (device in (SELECT device.device FROM device INNER JOIN service_device ON device_fk=device " +
                        "INNER JOIN service_order ON service_order_fk = service_order " +
                        "INNER JOIN service_request ON service_request_fk = service_request WHERE customer_fk = ANY (?)))";
                clientList = connection.createArrayOf("numeric", clientIds);
            }

            statement = connection.prepareStatement(sql);
            if (clientList != null) {
                setClientList(statement, clientList);
            }
            setParameters(device, statement);
            rs = statement.executeQuery();

            return buildDeviceList(rs);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            close(rs, statement, connection);
        }
    }

    public void setClientList(PreparedStatement statement, Array clientList) throws SQLException {
        statement.setArray(13, clientList);
        clientList.free();
    }

    public void setParameters(Device device, PreparedStatement statement) throws SQLException {
        statement.setInt(1, device.getDt().getDevice_type());
        statement.setInt(2, device.getDt().getDevice_type());
        statement.setString(3, device.getName());
        statement.setString(4, "%" + device.getName() + "%");
        statement.setString(5, device.getReg_no());
        statement.setString(6, "%" + device.getReg_no() + "%");
        statement.setString(7, device.getDescription());
        statement.setString(8, "%" + device.getDescription() + "%");
        statement.setString(9, device.getModel());
        statement.setString(10, "%" + device.getModel() + "%");
        statement.setString(11, device.getManufacturer());
        statement.setString(12, "%" + device.getManufacturer() + "%");
    }

    private List<Device> buildDeviceList(ResultSet rs) throws SQLException {
        List<Device> devices = new ArrayList<>();
        while (rs.next()) {
            Device d = new Device();
            DeviceType dt = new DeviceType();

            d.setDevice(rs.getInt(DEVICE));
            d.setName(rs.getString(NAME));
            d.setReg_no(rs.getString(REG_NO));
            d.setDescription(rs.getString(DESCRIPTION));
            d.setModel(rs.getString(MODEL));
            d.setManufacturer(rs.getString(MANUFACTURER));

            dt.setDevice_type(rs.getInt(DEVICE_TYPE_FK));
            d.setDt(dt);

            devices.add(d);
        }
        return devices;
    }
}
