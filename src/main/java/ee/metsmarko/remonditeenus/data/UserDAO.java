package ee.metsmarko.remonditeenus.data;

import ee.metsmarko.remonditeenus.model.SubjectType;
import ee.metsmarko.remonditeenus.model.User;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by Marko on 21.05.2015.
 * Deals with database operations of User object
 */
@Component
public class UserDAO extends DataAccessObject {

    private static final String ID = "user_account";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "passw";
    private static final String SUBJECTID = "subject_fk";
    private static final String ROLEID = "subject_type_fk";
    private static final String ROLENAME = "type_name";

    @SneakyThrows
    public User getUserByUsername(String name) {
        String sql = "SELECT * FROM user_account" +
                " INNER JOIN subject_type ON subject_type_fk = subject_type" +
                " WHERE username = ?";
        return makeGetUserByUsernameQuery(sql, name);
    }

    @SneakyThrows
    private User makeGetUserByUsernameQuery(String sql, String name) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, name);
            return executeGetUserByUsername(statement);
        }
    }

    @SneakyThrows
    private User executeGetUserByUsername(PreparedStatement statement) {
        try (ResultSet rs = statement.executeQuery()) {
            if (!rs.next()) {
                return null;
            }
            return buildUser(rs);
        }
    }

    @SneakyThrows
    private User buildUser(ResultSet rs) {
        User u = new User();
        u.setId(rs.getInt(ID));
        u.setSubject_fk(rs.getInt(SUBJECTID));
        u.setName(rs.getString(USERNAME));
        u.setPassword(rs.getString(PASSWORD));

        u.setType(buildUserType(rs));
        return u;
    }

    @SneakyThrows
    private SubjectType buildUserType(ResultSet rs) {
        SubjectType type = new SubjectType();
        type.setId(rs.getInt(ROLEID));
        type.setName(rs.getString(ROLENAME));
        return type;
    }

}
