package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by karmo on 22.05.15.
 */
@Entity
@Table(name = "subject_type")
@Data
@EqualsAndHashCode(of = "id")
public class SubjectType {
    @Id
    @Column(name = "subject_type")
    private Integer id;
    @Column(name = "type_name")
    private String name;
}
