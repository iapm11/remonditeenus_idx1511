package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Marko on 20.05.2015.
 */
@Entity
@Table(name = "service_request")
@Data
@EqualsAndHashCode(of = "service_request")
public class ServiceRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer service_request;
    private int service_request_status_type_fk, created_by;
    @Range(min = 1, message = "Klient on valimata")
    private int customer_fk;
    private Date created;
    @Length(min = 15, message = "Kirjeldus peab olema vähemalt 15 tähemärki pikk")
    private String service_desc_by_customer;
    @Length(min = 15, message = "Kirjeldus peab olema vähemalt 15 tähemärki pikk")
    private String service_desc_by_employee;
    @Transient
    private String customer_name;
}
