package ee.metsmarko.remonditeenus.model;

/**
 * Created by Marko on 20.05.2015.
 */

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "device")
@Data
@EqualsAndHashCode(of = "device")
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int device;

    @NotNull
    private String name;
    @NotNull
    private String reg_no;
    @NotNull
    private String model;
    @NotNull
    private String manufacturer;
    private String description;
    @ManyToOne
    @JoinColumn(name = "device_type_fk")
    @NotNull
    private DeviceType dt;
    @Transient
    private String customer_name;
}
