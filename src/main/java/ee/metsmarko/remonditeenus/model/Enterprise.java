package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by karmo on 23.05.15.
 */
@Entity
@Table(name = "enterprise")
@Data
@EqualsAndHashCode(of = "id")
public class Enterprise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "enterprise")
    private Integer id;
    private String name, full_name;
    private Date created, updated;
    private Integer created_by, updated_by;
}
