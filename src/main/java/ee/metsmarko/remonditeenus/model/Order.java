package ee.metsmarko.remonditeenus.model;

import ee.metsmarko.remonditeenus.enums.OrderStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.*;

/**
 * Created by karmo on 24.05.15.
 */
@Entity
@Table(name = "service_order")
@Data
@EqualsAndHashCode(of = "id")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_order")
    private Integer id;
    private Date created, updated, status_changed;
    @Column(updatable = false, insertable = false)
    private Double price_total;
    private String note;

    @Transient
    private String customer_name;

    @ManyToOne
    @JoinColumn(name = "service_request_fk", nullable = false)
    private ServiceRequest service_request;
    @ManyToOne
    @JoinColumn(name = "so_status_type_fk")
    private OrderStatus status;
    @ManyToOne
    @JoinColumn(name = "created_by")
    private Employee created_by;
    @ManyToOne
    @JoinColumn(name = "updated_by")
    private Employee updated_by;
    @ManyToOne
    @JoinColumn(name = "status_changed_by")
    private Employee status_changed_by;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "service_order_fk")
    private Set<ServiceDevice> serviceDevices = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "service_order_fk")
    private Set<Part> parts = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "service_order_fk")
    private Set<Action> actions = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "service_order_fk")
    private Set<Note> notes = new HashSet<>();

    public boolean hasPrice() {
        return OrderStatusEnum.HINNASTATUD.equals(getStatus())
                || OrderStatusEnum.ARVE_TEHTUD.equals(getStatus())
                || OrderStatusEnum.SEADE_TAGASTATUD.equals(getStatus());
    }

    public boolean hasInvoice() {
        return OrderStatusEnum.ARVE_TEHTUD.equals(getStatus())
                || OrderStatusEnum.SEADE_TAGASTATUD.equals(getStatus());
    }
}
