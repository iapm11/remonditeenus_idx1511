package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by karmo on 23.05.15.
 */
@Data
@EqualsAndHashCode(of = "id")
public class Client {
    private int id;
    private String name;

    public Client(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Client(int id, Person person) {
        this(id, person.getFirst_name() + " " + person.getLast_name());
    }

    public Client(int id, Enterprise enterprise) {
        this(id, enterprise.getFull_name());
    }
}
