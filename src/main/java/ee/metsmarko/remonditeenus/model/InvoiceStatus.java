package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by Marko on 4.06.2015.
 */
@Entity
@Table(name = "invoice_status_type")
@Data
@EqualsAndHashCode(of = "invoice_status_type")
public class InvoiceStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer invoice_status_type;
    private String type_name;
}
