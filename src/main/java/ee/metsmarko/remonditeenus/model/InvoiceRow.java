package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 *
 * Created by Marko on 4.06.2015.
 */
@Entity
@Table(name = "invoice_row")
@Data
@EqualsAndHashCode(of = "invoice_row")
public class InvoiceRow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer invoice_row;
    @ManyToOne
    @JoinColumn(name = "invoice_fk")
    private Invoice invoice;
    @ManyToOne
    @JoinColumn(name = "service_action_fk")
    private Action serviceAction;
    @ManyToOne
    @JoinColumn(name = "service_part_fk")
    private Part servicePart;
    private String action_part_description, unit_type;
    private double price_total, unit_price, amount;
    private int invoice_row_type;
}
