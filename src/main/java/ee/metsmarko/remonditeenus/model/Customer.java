package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by karmo on 26.05.15.
 */
@Entity
@Data
@EqualsAndHashCode(of = "id")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer")
    private Integer id;
    private Integer subject_fk;
    @ManyToOne
    @JoinColumn(name = "subject_type_fk")
    private SubjectType type;
}
