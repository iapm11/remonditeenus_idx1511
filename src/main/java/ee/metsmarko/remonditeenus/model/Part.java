package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by karmo on 27.05.15.
 */
@Entity
@Table(name = "service_part")
@Data
@EqualsAndHashCode(of = "id")
public class Part {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_part")
    private Integer id;
    private Integer service_order_fk;
    private Integer part_count;
    private String part_name, serial_no;
    private double part_price;
    private Date created;

    @ManyToOne
    @JoinColumn(name = "created_by")
    private Employee created_by;

    @ManyToOne
    @JoinColumn(name = "service_device_fk")
    private Device device;

    public boolean hasSerialNo() {
        return StringUtils.isNotBlank(getSerial_no());
    }
}
