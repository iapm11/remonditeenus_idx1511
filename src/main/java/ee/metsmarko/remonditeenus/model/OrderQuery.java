package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by karmo on 5.06.15.
 */
@Data
@EqualsAndHashCode(of = {"clientName", "deviceName", "deviceModel", "note"})
public class OrderQuery {
    private String clientName, deviceName, deviceModel, note;
}
