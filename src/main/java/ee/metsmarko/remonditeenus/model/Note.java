package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by karmo on 27.05.15.
 */
@Entity
@Table(name = "service_note")
@Data
@EqualsAndHashCode(of = "id")
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="service_note")
    private Integer id;
    private Date created;
    private String note;
    private Integer note_author_type, service_order_fk;

    @ManyToOne
    @JoinColumn(name = "customer_fk")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "employee_fk")
    private Employee employee;
}
