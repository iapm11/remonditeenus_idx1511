package ee.metsmarko.remonditeenus.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by karmo on 25.05.15.
 */
@Entity
@Table(name = "so_status_type")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class OrderStatus implements BaseStatusModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "so_status_type")
    private Integer id;
    private String type_name;
}
