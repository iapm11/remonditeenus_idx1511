package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by karmo on 27.05.15.
 */
@Entity
@Table(name = "service_action_status_type")
@Data
@EqualsAndHashCode(of = "id")
public class ActionStatus implements BaseStatusModel{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_action_status_type")
    private Integer id;
    private String type_name;
}
