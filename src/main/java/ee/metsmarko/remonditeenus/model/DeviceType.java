package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * Created by Marko on 20.05.2015.
 */
@Entity
@Table(name = "device_type")
@Data
@EqualsAndHashCode(of = "device_type")
public class DeviceType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int device_type;
    private int level;
    private String type_name;
    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "super_type_fk")
    private DeviceType dt;
}
