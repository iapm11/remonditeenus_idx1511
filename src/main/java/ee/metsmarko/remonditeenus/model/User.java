package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by Marko on 21.05.2015.
 * Registered user that can log in to the system.
 */
@Entity
@Table(name = "user_account")
@Data
@EqualsAndHashCode(of = "id")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_account")
    private Integer id;
    private Integer subject_fk;
    @ManyToOne
    @JoinColumn(name = "subject_type_fk")
    private SubjectType type;
    @Column(name = "username")
    private String name;
    @Column(name = "passw")
    private String password;

    public int getTypeId() {
        return getType().getId();
    }
}
