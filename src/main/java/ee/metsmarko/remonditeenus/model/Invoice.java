package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Marko on 4.06.2015.
 */
@Entity
@Table(name = "invoice")
@Data
@EqualsAndHashCode(of = "invoice")
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer invoice;
    @ManyToOne
    @JoinColumn(name = "invoice_status_type_fk")
    private InvoiceStatus invoiceStatus;
    @OneToOne
    @JoinColumn(name = "service_order_fk")
    private Order order;

    @ManyToOne
    @JoinColumn(name = "customer_fk")
    private Customer customer;

    private Date invoice_date, due_date, payment_date;
    private Double price_total;
    private String receiver_name, reference_number, receiver_accounts, description;

    @Transient
    private List<InvoiceRow> invoiceRows;
}
