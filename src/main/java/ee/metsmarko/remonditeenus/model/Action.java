package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by karmo on 27.05.15.
 */
@Entity
@Table(name = "service_action")
@Data
@EqualsAndHashCode(of = "id")
public class Action {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_action")
    private Integer id;
    private Integer service_order_fk;
    private Double service_amount, price;
    private Date created;
    private String action_description;

    @ManyToOne
    @JoinColumn(name = "created_by")
    private Employee created_by;

    @ManyToOne
    @JoinColumn(name = "service_device_fk")
    private Device device;

    @ManyToOne
    @JoinColumn(name = "service_action_status_type_fk")
    private ActionStatus status;

    @ManyToOne
    @JoinColumn(name = "service_type_fk")
    private ServiceType type;

    public Double getPrice() {
        if (price == null && type != null)
            return type.getService_price();
        return price;
    }
}
