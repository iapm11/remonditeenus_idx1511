package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by karmo on 27.05.15.
 */
@Entity
@Data
@EqualsAndHashCode
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee")
    private Integer id;
    private Integer struct_unit_fk;
    private String active;

    @ManyToOne
    @JoinColumn(name = "person_fk")
    private Person person;

    @ManyToOne
    @JoinColumn(name = "enterprise_fk")
    private Enterprise enterprise;
}
