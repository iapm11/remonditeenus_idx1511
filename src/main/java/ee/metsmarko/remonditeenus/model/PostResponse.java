package ee.metsmarko.remonditeenus.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Map;

/**
 * Created by karmo on 28.05.15.
 */
@Getter
@EqualsAndHashCode(of = {"success", "message", "errors"})
public class PostResponse {
    private boolean success;
    private String message;
    Map<String, String> errors;

    public PostResponse() {
        success = true;
    }

    public PostResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public PostResponse(String message, Map<String, String> errors) {
        this(false, message);
        this.errors = errors;
    }
}
