package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by karmo on 26.05.15.
 */
@Entity
@Table(name = "service_device")
@Data
@EqualsAndHashCode(of = "id")
public class ServiceDevice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_device")
    private Integer id;
    private Integer service_order_fk;
    private Date to_store, from_store, status_changed;

    @ManyToOne
    @JoinColumn(name = "device_fk")
    private Device device;

    @ManyToOne
    @JoinColumn(name = "service_device_status_type_fk")
    private ServiceDeviceStatus status;

    @Transient
    private List<Action> actions;
    @Transient
    private List<Part> parts;
}
