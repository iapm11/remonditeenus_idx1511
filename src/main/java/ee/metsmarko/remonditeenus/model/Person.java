package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by karmo on 23.05.15.
 */
@Entity
@Table(name = "person")
@Data
@EqualsAndHashCode(of = "id")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "person")
    private Integer id;
    private String first_name, last_name, identity_code;
    private Date birth_date, created, updated;
    private Integer created_by, updated_by;
}
