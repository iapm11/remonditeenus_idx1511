package ee.metsmarko.remonditeenus.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * Created by Salome on 29.10.2015.
 */
@Entity
@Table(name = "service_request_status_type")
@Data
@EqualsAndHashCode(of = "id")
public class RequestStatus implements BaseStatusModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_request_status_type")
    private Integer id;
    private String type_name;
}


