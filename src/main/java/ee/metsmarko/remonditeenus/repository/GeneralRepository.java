package ee.metsmarko.remonditeenus.repository;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 *
 * Created by mihkelk on 19.11.2015.
 */
@Repository
@Transactional
public class GeneralRepository<T> implements IGeneralRepository<T> {

    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }


    @SuppressWarnings("unchecked")
    public T findById(int id, Class clazz) {
        return (T) getSession().get(clazz, id);
    }

    public void add(T item) {
        getSession().save(item);
    }

    public T merge(T item) {
        getSession().merge(item);
        return item;
    }

    public void remove(T item) {
        getSession().delete(item);
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll(Class clazz) {
        return getSession().createCriteria(clazz).list();
    }

    @SuppressWarnings("unchecked")
    public List<T> findByQuery(String query, Param... params) {

        Query q = getSession().createQuery(query);
        for (Param p : params) {
            setParam(q, p);
        }
        return q.list();
    }

    private void setParam(Query query, Param param) {
        if (param.value instanceof Collection) {
            query.setParameterList(param.name, (Collection) param.value);
        } else {
            query.setParameter(param.name, param.value);
        }
    }
}
