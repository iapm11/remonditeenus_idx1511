package ee.metsmarko.remonditeenus.repository;

/**
 * Created by Karmo on 020 20 05 2015.
 */
public class Param {
    public String name;
    public Object value;

    public Param(String name, Object value) {
        this.name = name;
        this.value = value;
    }
}
