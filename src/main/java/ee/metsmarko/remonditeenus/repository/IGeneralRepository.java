package ee.metsmarko.remonditeenus.repository;

import java.util.List;

/**
 * Created by mihkelk on 19.11.2015.
 */
public interface IGeneralRepository<T> {

    T findById(int id, Class clazz);

    void add(T item);

    T merge(T item);

    void remove(T item);

    List<T> findAll(Class clazz);

    List<T> findByQuery(String query, Param... params);

}
