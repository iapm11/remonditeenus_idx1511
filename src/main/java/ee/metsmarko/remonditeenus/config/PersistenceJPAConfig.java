package ee.metsmarko.remonditeenus.config;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:/DBConnection.properties")
@ImportResource({ "classpath:/aop-conf.xml" })
@ComponentScan({ "ee.metsmarko.remonditeenus" })
public class PersistenceJPAConfig{

    @Autowired
    private Environment env;

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("ee.metsmarko.remonditeenus");
        sessionFactory.setHibernateProperties(additionalProperties());
        sessionFactory.setNamingStrategy(new ImprovedNamingStrategy());
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        if (env.acceptsProfiles("test")) {

            String servername = env.getProperty("test-servername");
            String user = env.getProperty("test-username");
            String password = env.getProperty("test-password");
            String dbname = env.getProperty("test-dbname");
            String port = env.getProperty("test-port");
            return getDataSource(servername, user, password, dbname, port);
        }
        String servername = env.getProperty("dev-servername");
        String user = env.getProperty("dev-username");
        String password = env.getProperty("dev-password");
        String dbname = env.getProperty("dev-dbname");
        String port = env.getProperty("dev-port");

        return getDataSource(servername, user, password, dbname, port);
    }

    private DataSource getDataSource(String servername, String user, String password, String dbname, String port) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");

        dataSource.setUrl("jdbc:postgresql://" + servername + ":" + port + "/" + dbname + "");
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);
        return txManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
        return new PersistenceExceptionTranslationPostProcessor();
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
        properties.setProperty("hibernate.globally_quoted_identifiers", "false");
        properties.setProperty("hibernate.id.new_generator_mappings", "true");
        properties.setProperty("hibernate.format_sql", "true");

        return properties;
    }
}
