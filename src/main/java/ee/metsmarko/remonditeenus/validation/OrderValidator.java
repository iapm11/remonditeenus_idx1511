package ee.metsmarko.remonditeenus.validation;

import ee.metsmarko.remonditeenus.enums.ServiceDeviceStatusEnum;
import ee.metsmarko.remonditeenus.model.Order;
import ee.metsmarko.remonditeenus.model.Part;
import ee.metsmarko.remonditeenus.model.ServiceDevice;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by karmo on 30.05.15.
 */
public class OrderValidator {

    private Order order;
    private Map<String, String> errors;

    public OrderValidator(Order updated) {
        this.order = updated;
    }

    public Map<String, String> validateUpdate() {
        errors = new HashMap<>();
        if (order.hasPrice())
            checkForUnfinishedDevices();
        checkPartSerialNoAndCount();
        return errors;
    }

    private void checkForUnfinishedDevices() {
        for (ServiceDevice sd : order.getServiceDevices()) {
            if (ServiceDeviceStatusEnum.VASTU_VOETUD.equals(sd.getStatus())) {
                errors.put("status", "Ei saa staatust muuta, sest töö kõigi " +
                        "seadmetega pole veel lõpetatud.");
            }
        }
    }

    private void checkPartSerialNoAndCount() {
        for (Part p : order.getParts()) {
            if (p.hasSerialNo() && partCountIsNotOne(p)) {
                errors.put("parts.serial_no", "Kui varuosa seerianumber on " +
                        "märgitud, siis peab koguseks olema 1.");
            }
        }
    }

    private boolean partCountIsNotOne(Part p) {
        return p.getPart_count() == null || p.getPart_count() != 1;
    }

    public Map<String, String> validateDelete() {
        errors = new HashMap<>();
        if (order.hasInvoice())
            errors.put("status", "Ei saa kustutada tellimust, millele on arve tehtud");
        return errors;
    }
}
