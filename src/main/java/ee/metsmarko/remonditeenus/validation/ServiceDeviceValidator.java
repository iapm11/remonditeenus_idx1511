package ee.metsmarko.remonditeenus.validation;

import ee.metsmarko.remonditeenus.enums.ActionStatusEnum;
import ee.metsmarko.remonditeenus.model.Action;
import ee.metsmarko.remonditeenus.model.ServiceDevice;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by karmo on 3.06.15.
 */
public class ServiceDeviceValidator {

    private ServiceDevice serviceDevice;
    private Map<String, String> errors;

    public ServiceDeviceValidator(ServiceDevice serviceDevice) {
        this.serviceDevice = serviceDevice;
    }

    public Map<String, String> validateUpdate() {
        errors = new HashMap<>();
        if (serviceDevice.getStatus().getId() >= 2)
            checkThatNoActionIsIncomplete();
        return errors;
    }

    private void checkThatNoActionIsIncomplete() {
        for (Action a : serviceDevice.getActions()) {
            if (ActionStatusEnum.POOLELI.equals(a.getStatus())) {
                errors.put("status", "Ei saa staatust muuta. " +
                        "Seadmega on seotud lõpetamata töid.");
            }
        }
    }

    public Map<String, String> validateDelete() {
        errors = new HashMap<>();
        if (!serviceDevice.getActions().isEmpty()
                || !serviceDevice.getParts().isEmpty())
            errors.put("remove", "Ei saa kustutada seadet, " +
                    "millega on seotud töö või varuosa.");
        return errors;
    }
}
