package ee.metsmarko.remonditeenus.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by Marko on 21.05.2015.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    public void configAuthBuilder(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(userDetailService);
        /*builder.inMemoryAuthentication().withUser("emp").password("emp").roles("EMPLOYEE");
        builder.inMemoryAuthentication().withUser("cli").password("cli").roles("CLIENT");*/
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/request/**").hasAuthority("EMPLOYEE")
                .antMatchers("/order").hasAuthority("EMPLOYEE")
                .antMatchers("/order/my/**").hasAuthority("CLIENT")
                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and().formLogin();
    }
}
