package ee.metsmarko.remonditeenus.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by Marko on 21.05.2015.
 */
public class SecurityWebAppInitializer extends AbstractSecurityWebApplicationInitializer {
}
