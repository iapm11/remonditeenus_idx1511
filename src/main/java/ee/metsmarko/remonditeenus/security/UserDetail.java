package ee.metsmarko.remonditeenus.security;

import ee.metsmarko.remonditeenus.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Object that holds logged in user's data
 */
public class UserDetail implements UserDetails {

    private User user;

    private final static String COMPANY = "COMPANY";
    private final static String EMPLOYEE = "EMPLOYEE";
    private final static String CLIENT = "CLIENT";
    private final static int COMPANY_CODE = 2;
    private final static int EMPLOYEE_CODE = 3;
    private final static int CLIENT_CODE = 4;

    public UserDetail(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        GrantedAuthority authority = new GrantedAuthority() {
            @Override
            public String getAuthority() {
                if(user.getTypeId() == CLIENT_CODE){
                    return CLIENT;
                }
                if(user.getTypeId() == COMPANY_CODE){
                    return COMPANY;
                }
                if(user.getTypeId() == EMPLOYEE_CODE){
                    return EMPLOYEE;
                }
                return null;
            }
        };
        authorities.add(authority);
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getUser() {
        return user;
    }
}
