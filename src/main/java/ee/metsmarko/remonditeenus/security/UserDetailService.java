package ee.metsmarko.remonditeenus.security;

import ee.metsmarko.remonditeenus.model.User;
import ee.metsmarko.remonditeenus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Marko on 21.05.2015.
 */
@Service
public class UserDetailService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = userService.getUserByName(name);
        if(user == null){
            throw new UsernameNotFoundException("Username " + name + " not found!");
        }
        return new UserDetail(user);
    }
}
