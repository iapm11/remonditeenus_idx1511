package ee.metsmarko.remonditeenus.enums;

import ee.metsmarko.remonditeenus.model.BaseStatusModel;
import lombok.Getter;

/**
 * Created by karmo on 17.11.15.
 */
public enum NoteAuthorTypeEnum {
    KLIENT(1),
    TOOTAJA(2);

    NoteAuthorTypeEnum(Integer id) {
        this.id = id;
    }

    @Getter
    private Integer id;
}