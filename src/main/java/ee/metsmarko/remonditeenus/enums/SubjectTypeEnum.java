package ee.metsmarko.remonditeenus.enums;

import ee.metsmarko.remonditeenus.model.BaseStatusModel;
import ee.metsmarko.remonditeenus.model.SubjectType;

/**
 * Created by Karmo on 010 10 11 2015.
 */
public enum SubjectTypeEnum {
    ISIK(1),
    ETTEVOTTE(2),
    TOOTAJA(3),
    KLIENT(4);


    SubjectTypeEnum(Integer id) {
        this.id = id;
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public boolean equals(BaseStatusModel model){
        return ((SubjectType) model).getId().equals(getId());
    }
}
