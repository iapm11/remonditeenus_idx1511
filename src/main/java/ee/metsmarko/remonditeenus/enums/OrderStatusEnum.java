package ee.metsmarko.remonditeenus.enums;

import ee.metsmarko.remonditeenus.model.BaseStatusModel;
import ee.metsmarko.remonditeenus.model.OrderStatus;

/**
 *
 * Created by mihkelk on 18.10.2015.
 */
public enum OrderStatusEnum {
    VASTU_VOETUD(1),
    VALMIS(2),
    HINNASTATUD(3),
    ARVE_TEHTUD(4),
    SEADE_TAGASTATUD(5);

    OrderStatusEnum(Integer id) {
        this.id = id;
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public boolean equals(BaseStatusModel model){
        return ((OrderStatus) model).getId().equals(getId());
    }
}