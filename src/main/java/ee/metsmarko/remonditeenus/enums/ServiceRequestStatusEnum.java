package ee.metsmarko.remonditeenus.enums;

import ee.metsmarko.remonditeenus.model.BaseStatusModel;
import ee.metsmarko.remonditeenus.model.RequestStatus;

/**
 * Created by Salome on 29.10.2015.
 */
public enum ServiceRequestStatusEnum {
    REGISTREERITUD(1),
    TAGASI_LYKATUD(2),
    TELLIMUS_TEHTUD(3);

    ServiceRequestStatusEnum(Integer id) {
        this.id = id;
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public boolean equals(BaseStatusModel model){
        return ((RequestStatus) model).getId().equals(getId());
    }
}
