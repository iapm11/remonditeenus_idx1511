package ee.metsmarko.remonditeenus.enums;

import ee.metsmarko.remonditeenus.model.ActionStatus;
import ee.metsmarko.remonditeenus.model.BaseStatusModel;
import ee.metsmarko.remonditeenus.model.OrderStatus;

/**
 *
 * Created by mihkelk on 18.10.2015.
 */
public enum ActionStatusEnum {
    POOLELI(1),
    VALMIS(2);

    ActionStatusEnum(Integer id) {
        this.id = id;
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public boolean equals(BaseStatusModel model){
        return ((ActionStatus) model).getId().equals(getId());
    }
}