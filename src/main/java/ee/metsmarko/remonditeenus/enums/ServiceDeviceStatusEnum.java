package ee.metsmarko.remonditeenus.enums;

import ee.metsmarko.remonditeenus.model.ActionStatus;
import ee.metsmarko.remonditeenus.model.BaseStatusModel;
import ee.metsmarko.remonditeenus.model.ServiceDeviceStatus;

/**
 *
 * Created by mihkelk on 18.10.2015.
 */
public enum ServiceDeviceStatusEnum {
    VASTU_VOETUD(1),
    TOO_SEADMEGA_LOPETATUD(2),
    SEADE_KLIENDILE_TAGASTATUD(3);

    ServiceDeviceStatusEnum(Integer id) {
        this.id = id;
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public boolean equals(BaseStatusModel model){
        return ((ServiceDeviceStatus) model).getId().equals(getId());
    }
}