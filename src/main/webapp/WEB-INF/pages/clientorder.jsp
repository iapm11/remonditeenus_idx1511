<%--
  Created by IntelliJ IDEA.
  User: Marko
  Date: 31.05.2015
  Time: 13:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tellimus</title>
    <script src="/rteenus/resources/js/lib/jquery-2.1.3.js"></script>
    <script src="/rteenus/resources/js/clientorder.js"></script>
    <link rel="stylesheet" href="/rteenus/resources/css/style.css">
    <link rel="stylesheet" href="/rteenus/resources/css/order.css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
</head>
<body>
<%@include file="menu.jsp" %>
<div class="order">
    <table>
        <tr>
            <td>Tellimuse number:</td>
            <td><c:out value="${order.id}"/></td>
        </tr>
        <tr>
            <td>Tellimus sisestatud:</td>
            <td><c:out value="${order.created}"/></td>
        </tr>
        <tr>
            <td>Tellimuse viimane muutmine:</td>
            <td><c:out value="${order.updated}"/></td>
        </tr>
        <tr>
            <td>Tellimuse kirjeldus:</td>
            <td><c:out value="${order.service_request.service_desc_by_employee}"/></td>
        </tr>
        <tr>
            <br>
            <td>KOMMENTAARID</td>
            <td></td>
        </tr>
        <c:forEach var="note" items="${order.notes}">
            <tr>
                <td>
                    <c:choose>
                        <c:when test="${note.note_author_type == 1}">
                            ${order.customer_name}
                        </c:when>
                        <c:when test="${note.note_author_type == 2}">
                            ${note.employee.person.first_name}  ${note.employee.person.last_name} (Töötaja)
                        </c:when>
                    </c:choose>
                </td>
                <td>${note.note}</td>
            </tr>
        </c:forEach>
    </table>
    <br>
    Lisa märkus<br>
    <textarea id="note_text" cols="40" rows="8"></textarea> <br>
    <button type="button" onclick="addCustomerNote(${order.id})">Lisa</button>
</div>
<c:if test="${not empty invoice}">
    <div class="invoice" style="width:500px; margin-left: 250px">
        <hr>
        <h3>Arve</h3>
        <table>
            <tr>
                <td><b>Saaja:</b></td>
                <td>${invoice.receiver_name}</td>
            </tr>
            <tr>
                <td><b>Arve kinnitatud:</b></td>
                <td>${invoice.invoice_date}</td>
            </tr>
            <tr>
                <td><b>Arve makse tähtaeg*:</b></td>
                <td>${invoice.due_date}</td>
            </tr>
            <tr>
                <td><b>Maksmisele kuuluv summa:</b></td>
                <td>${invoice.price_total} &euro;</td>
            </tr>
            <tr>
                <td><b>Viitenumber:</b></td>
                <td>${invoice.reference_number}</td>
            </tr>
            <tr>
                <td><b>Rekvisiidid*:</b></td>
                <td>${invoice.receiver_accounts}</td>
            </tr>
            <tr>
                <td><b>Kirjeldus:</b></td>
                <td>${invoice.description}</td>
            </tr>
        </table>
        <hr>
        <h4>Hind sisaldab järgnevaid kulutusi:</h4>
        <table class="bordered">
            <tr>
                <th>Kirjeldus</th>
                <th>Kogus</th>
                <th>Ühe ühiku hind</th>
                <th>Kogu hind</th>
                <th>Ühik</th>
            </tr>
            <c:forEach var="row" items="${invoice.invoiceRows}">
                <tr>
                    <td>${row.action_part_description}</td>
                    <td>${row.amount}</td>
                    <td>${row.unit_price}</td>
                    <td>${row.price_total}</td>
                    <td>${row.unit_type}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
</c:if>
</body>
</html>
