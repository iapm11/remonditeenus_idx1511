<%--
  Created by IntelliJ IDEA.
  User: Marko
  Date: 30.05.2015
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link rel="stylesheet" href="/rteenus/resources/css/style.css">
    <title>Vaata enda tellimusi</title>
</head>
<body>
<%@include file="menu.jsp" %>
<c:forEach var="order" items="${orders}">
    <div class="order">
        <table>
            <tr>
                <td>Tellimuse number:</td>
                <td><c:out value="${order.id}"/></td>
            </tr>
            <tr>
                <td>Tellimus sisestatud:</td>
                <td><c:out value="${order.created}"/></td>
            </tr>
            <tr>
                <td>Tellimuse viimane muutmine:</td>
                <td><c:out value="${order.updated}"/></td>
            </tr>
            <tr>
                <td>Tellimuse kirjeldus:</td>
                <td><c:out value="${order.service_request.service_desc_by_employee}"/></td>
            </tr>
        </table>
        <a href="/rteenus/order/my/${order.id}">
            <button type="button">Vaata lähemalt</button>
        </a>
    </div>
    <br>
</c:forEach>
</body>
</html>
