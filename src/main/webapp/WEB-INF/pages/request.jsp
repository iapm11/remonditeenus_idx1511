<%--
  Created by IntelliJ IDEA.
  User: Marko
  Date: 24.05.2015
  Time: 20:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Pöördumine</title>
    <meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <link rel="stylesheet" href="/rteenus/resources/css/style.css">
    <script src="/rteenus/resources/js/lib/jquery-2.1.3.js"></script>
    <script src="/rteenus/resources/js/request.js" charset="utf-8"></script>
</head>
<body>
<%@include file="menu.jsp"%>
<table class="request" style="float:left">
    <c:if test="${not empty request}">
        Kliendi pöördumine:
        <tr>
            <td>Id</td>
            <td><c:out value="${request.service_request}"/></td>
        </tr>

        <tr>
            <td>Kuupäev</td>
            <td><c:out value="${request.created}"/></td>
        </tr>

        <tr>
            <td>Kliendi mure</td>
            <td><c:out value="${request.service_desc_by_customer}"/></td>
        </tr>
        <tr>
            <td>Töötaja hinnang</td>
            <td><c:out value="${request.service_desc_by_employee}"/></td>
        </tr>
        <br>
        <tr>
            <td>
                <button onclick="showAddDeviceScreen()">Lisa seadmed</button>
                <div class="addeddevices"></div>
                <br>
                <br>
                <button onclick="cancelRequest(${request.service_request})">Ei võta vastu</button>
                <button type="button" onclick="submitOrder(${request.service_request})">Tee tellimus</button>
            </td>
        </tr>
    </c:if>
</table>
<%@include file="adddevice.html"%>
</body>
</html>
