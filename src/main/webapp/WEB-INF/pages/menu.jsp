<%--
  Created by IntelliJ IDEA.
  User: Marko
  Date: 30.05.2015
  Time: 16:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<div class="menu">
    <a href="/rteenus/">Avalehele</a><br>
    ------------------------<br>
    <sec:authorize access="hasAuthority('EMPLOYEE')">
        <a id = "lisa_poordumine" href="/rteenus/request/add">Lisa pöördumine</a><br>
        <a href="/rteenus/request/">Otsi klientide pöördumisi</a><br>
        <a href="/rteenus/order#/search">Vaata tellimusi</a><br>
        <a href="http://hektor4.ttu.ee/tomcat_webapp_logs/rteenus/log.txt">
            Vaata logi</a><br>
    </sec:authorize>
    <sec:authorize access="not hasAuthority('EMPLOYEE')">
        <a href="/rteenus/order/my">Vaata enda tellimusi</a><br>
    </sec:authorize>
    ------------------------<br>
    <a href="/rteenus/logout">Logi välja</a>

</div>