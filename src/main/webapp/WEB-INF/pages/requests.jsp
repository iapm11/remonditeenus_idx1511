<%--
  Created by IntelliJ IDEA.
  User: Marko
  Date: 21.05.2015
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>--%>
<html>
<head>
    <meta charset="UTF-8">
    <script src="/rteenus/resources/js/lib/jquery-2.1.3.js"></script>
    <script src="/rteenus/resources/js/requestfinder.js" charset="utf-8"></script>
    <link rel="stylesheet" href="/rteenus/resources/css/style.css">
    <title>Pöördumised</title>
</head>
<body>
<%@include file="menu.jsp"%>

<h3>Registreeritud pöördumised, mis nõuavad otsekohest sekkumist töötajate poolt</h3>

    <div class="requestlist">

    </div>
</body>
</html>
