<%--
  Created by IntelliJ IDEA.
  User: Marko
  Date: 20.05.2015
  Time: 19:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <script src="/rteenus/resources/js/lib/jquery-2.1.3.js"></script>
    <script src="/rteenus/resources/js/userpicker.js"></script>
    <link rel="stylesheet" href="/rteenus/resources/css/style.css">
    <title>Register request</title>
</head>
<body>

<%@include file="menu.jsp"%>

<form:form action="" commandName="serviceRequest" method="POST" style="float: left; width: 500px">
    <h4 style="margin-top: 0">Sisesta uus kliendi pöördumine</h4>
    ${message}
    <table border="0" cellpadding="2" cellspacing="1">
        <tr style="display: none">
            <td><form:input path="customer_fk" id="customer_fk"/></td>
        </tr>
        <tr>
            <td style="background: lightgrey">Klient:</td>
            <td><form:input path="customer_name" id="customer_name" readonly="true"/>
                <button type="button" onclick="openSearch()">Otsi klienti</button>
            </td>
            <td><form:errors path="customer_fk" cssClass="error"/></td>
        </tr>
        <tr>
            <td style="background: lightgrey">Kliendi kirjeldus:</td>
            <td><form:textarea path="service_desc_by_customer"/></td>
            <td><form:errors path="service_desc_by_customer" cssClass="error"/></td>
        </tr>
        <tr>
            <td style="background: lightgrey">Töötaja kirjeldus:</td>
            <td><form:textarea path="service_desc_by_employee"/></td>
            <td><form:errors path="service_desc_by_employee" cssClass="error"/></td>
        </tr>
    </table>
    <button id="add_request" type="submit">Salvesta pöördumine</button>
</form:form>

<div class="userfinder">
    <input id="userfinder" type="text"/>
    <button type="button" onclick="findClient()">Otsi</button>
    <button type="button" onclick="closeSearch()">Sule</button>
</div>
</body>
</html>
