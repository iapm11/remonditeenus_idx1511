<%--
  Created by IntelliJ IDEA.
  User: karmo
  Date: 24.05.15
  Time: 19:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html ng-app="Order">
<head>
    <meta charset="UTF-8">
    <title>Tellimused</title>
    <link rel="stylesheet" href="/rteenus/resources/css/order.css">
    <link rel="stylesheet" href="/rteenus/resources/css/style.css">
</head>
<body ng-controller="OrderController">
<%@include file="menu.jsp"%>

<div class="orderwrapper">
    <ng-view></ng-view>
</div>

<script src="/rteenus/resources/js/lib/underscore.js"></script>
<script src="/rteenus/resources/js/lib/jquery-2.1.3.js"></script>
<script src="/rteenus/resources/js/lib/angular.js"></script>
<script src="/rteenus/resources/js/lib/angular-route.js"></script>
<script src="/rteenus/resources/js/request.js"></script>
<script src="/rteenus/resources/js/order/model/action.js"></script>
<script src="/rteenus/resources/js/order/model/orderquery.js"></script>
<script src="/rteenus/resources/js/order/model/part.js"></script>
<script src="/rteenus/resources/js/order/controller/order.js"></script>
<script src="/rteenus/resources/js/order/controller/search.js"></script>
<script src="/rteenus/resources/js/order/controller/detail.js"></script>
<script src="/rteenus/resources/js/order/controller/servicedevice.js"></script>
<script src="/rteenus/resources/js/order/service/dbservice.js"></script>
<script src="/rteenus/resources/js/order/service/orderservice.js"></script>
<script src="/rteenus/resources/js/order/service/invoiceservice.js"></script>
<script src="/rteenus/resources/js/order/service/servicedeviceservice.js"></script>
</body>
</html>
