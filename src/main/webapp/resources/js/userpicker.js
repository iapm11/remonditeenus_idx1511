function openSearch() {
    $('.userfinder').css('display', 'block');
}
function closeSearch() {
    $('.userfinder').css('display', 'none');
}
function findClient() {
    $.ajax({
        url: "/rteenus/client/findbyname/" + $('#userfinder').val(),
        context: document.body
    }).done(function (data) {
        showClients(data);
    });
}
function showClients(data) {
    $('.clients').remove();
    var clients = "<div class='clients'>";
    if (data.length == 0) {
        clients += "Klienti ei leitud"
    } else {
        $(jQuery.parseJSON(JSON.stringify(data))).each(function () {
            clients += this.name;
            clients += '&nbsp;<button id="klient_' + this.id + '" onclick="chooseClient(\'' + this.id + '\', \'' + this.name + '\')">Vali</button>';
            clients += "<br>";
        });
    }
    clients += "</div>";
    $('.userfinder').append(clients);
}

function chooseClient(id, name) {
    $('#customer_fk').attr('value', id);
    $('#customer_name').val(name);
}