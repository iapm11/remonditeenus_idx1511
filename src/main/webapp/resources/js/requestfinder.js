function getRequests(){
    $.ajax({
        url: "/rteenus/request/registered/all",
        context: document.body
    }).done(function (data) {
        showRequests(data);
    });
}

function showRequests(data) {
    $('.requests').remove();
    var requests = "<div class='requests'>";
    $(jQuery.parseJSON(JSON.stringify(data))).each(function () {
        requests += '<div class="request">';
        requests += "Pöördumise number:" + this.service_request + "<br>";
        requests += "Loodud:" + new Date(this.created) + "<br>";
        requests += "Kliendi mure: " + this.service_desc_by_customer + "<br>";
        requests += "Töötaja hinnang:" + this.service_desc_by_employee + "<br>";
        requests += '&nbsp;<a href="'+ this.service_request +'"><button>Vaata lähemalt</button></a>';
        requests += '</div>';
        requests += "<br>";
    });
    requests += "</div>";
    $('.requestlist').append(requests);
}
getRequests();