function addCustomerNote(id) {
    var data = constructPayLoad(id);
    $.ajax({
        url: "/rteenus/note/client/add",
        context: document.body,
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        data: JSON.stringify(data)
    }).success(function () {
        location.reload();
    })
}

function constructPayLoad(id) {
    return {
        'service_order_fk': id,
        'note': $('#note_text').val()
    }
}
