var devices = [];

function cancelRequest(id) {
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
    $.ajax({
        url: "/rteenus/request/" + id,
        context: document.body,
        method: "PUT"
    }).done(function (data) {
    });
}

function showAddDeviceScreen() {
    $('.adddevice').css("display", "block");
}

function showDeviceSearchForm() {
    $('.deviceSearchForm').css("display", "block");
    $('.deviceAddForm').css("display", "none");
}
function showDeviceAddForm() {
    $('.deviceAddForm').css("display", "block");
    $('.deviceSearchForm').css("display", "none");
}
function retreiveDeviceTypes() {
    $.ajax({
        url: "/rteenus/device/types",
        context: document.body
    }).done(function (data) {
        displayDeviceTypes(data);
    });
}

function displayDeviceTypes(data) {
    var selectBoxContent = "";
    $(jQuery.parseJSON(JSON.stringify(data))).each(function () {
        if (this.level != 1) {
            selectBoxContent += '<option value="' + this.device_type + '">';
            selectBoxContent += this.type_name;
            selectBoxContent += '</option>';
        }
    });
    $('#device_type').append(selectBoxContent);
    $('#add_device_type').append(selectBoxContent);
}

function searchDevice() {
    var device = constructDeviceSearchPayLoad();
    $.ajax({
        url: "/rteenus/device/find",
        context: document.body,
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        data: JSON.stringify(device)
    }).done(function (data) {
        displayFoundDevices(data);
    });
}

function constructDeviceSearchPayLoad() {
    return {
        'name': $('#name').val() ? $('#name').val() : undefined,
        'reg_no': $('#reg_no').val() ? $('#reg_no').val() : undefined,
        'model': $('#model').val() ? $('#model').val() : undefined,
        'dt': {
            'device_type': $('#device_type').val()
        },
        'customer_name': $('#client_name').val() ? $('#client_name').val() : undefined
    };
}

function displayFoundDevices(data) {
    $('.found-devices').remove();
    var devices = "<div class='found-devices'>Leitud:<br>";
    $(jQuery.parseJSON(JSON.stringify(data))).each(function () {
        devices += this.name + ' ' + this.model;
        devices += '&nbsp;<a href="#" onclick="addDeviceToOrder(' + this.device + ',\'' + this.name + '\'); return false;">Lisa seade tellimusse</a><br>';
    });
    devices += "</div>";
    $('.adddevice').append(devices);
}

function addDeviceToOrder(id, name) {
    if (devices.indexOf(id) == -1) {
        $('.addeddevices').append('<div id="device-' + id + '">' + name + '&nbsp;<a href="#" onclick="removeDeviceFromOrder(' + id + '); return false">Eemalda</a> </div>');
        devices.push(id);
    }
}

function removeDeviceFromOrder(id) {
    var index = devices.indexOf(id);
    devices.splice(index, 1);
    $('#device-'+id).remove();
}

function addDevice() {
    var device = constructDeviceAddPayLoad();
    $.ajax({
        url: "/rteenus/device/add",
        context: document.body,
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        data: JSON.stringify(device)
    }).success(function (data) {
        displayFoundAddedDevices(data);
        clearAddForm();
    }).error(function (data) {
        alert("Nõutud väljad täitmata");
    });
}

function constructDeviceAddPayLoad() {
    return {
        'name': $('#add_name').val() ? $('#add_name').val() : undefined,
        'reg_no': $('#add_reg_no').val() ? $('#add_reg_no').val() : undefined,
        'model': $('#add_model').val() ? $('#add_model').val() : undefined,
        'description': $('#add_description').val() ? $('#add_description').val() : undefined,
        'manufacturer': $('#add_manufacturer').val() ? $('#add_manufacturer').val() : undefined,
        'dt': {
            'device_type': $('#add_device_type').val()
        }
    };
}

function displayFoundAddedDevices(data) {
    var device = jQuery.parseJSON(JSON.stringify(data));
    $('.addeddevices').append("<div>" + device.name + "</div>");
    devices.push(device.device);
}
function clearAddForm() {
    $('#add_name').val("");
    $('#add_reg_no').val("");
    $('#add_model').val("");
    $('#add_description').val("");
    $('#add_manufacturer').val("");
}

function submitOrder(id) {
    if (devices.length == 0) {
        alert("Seadmeid ei ole valitud");
        return;
    }
    $.ajax({
        url: "/rteenus/order/add/" + id,
        context: document.body,
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        data: JSON.stringify(devices)
    }).success(function (data) {
        window.location.href = "/rteenus/order#/detail/" + data;
    })
}

retreiveDeviceTypes();
