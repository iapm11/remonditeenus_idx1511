/**
 * Created by karmo on 24.05.15.
 */
var search = angular.module("SearchOrders", ['ngRoute']);

search.controller("SearchOrdersController", function(
    $scope, $location, OrderService, SearchService) {

    $scope.query = new OrderQuery();

    if (SearchService.orders)
        $scope.orders = SearchService.orders;

    $scope.search = function() {
        OrderService.searchOrder($scope.query, function(data) {
            $scope.orders = data;
            $scope.$apply();
        });
    };

    $scope.open = function(index) {
        SearchService.orders = $scope.orders;
        var orderId = $scope.orders[index].id;
        $location.path("/detail/" + orderId);
    }
});

search.service('SearchService', function () {

});