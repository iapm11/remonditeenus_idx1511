/**
 * Created by karmo on 25.05.15.
 */
var detail = angular.module("OrderDetail", ['ngRoute'])

    .controller("DetailController", function ($scope, $location,
                                              $routeParams, OrderService, InvoiceService) {
        var orderId = parseInt($routeParams.orderId);

        $scope.invoice = {};
        $scope.invoice.customer = {};
        $scope.invoice.order = {};

        function init(data) {
            $scope.order = data;
            $scope.orderStatusInServer = data.status.id;
            $scope.selectedOrderStatus = data.status.id;
            for (var i = 0; i < $scope.order.actions.length; i++) {
                var action = $scope.order.actions[i];
                action.selectedTypeId = action.type.id;
            }
            $scope.loadInvoice();
        }
        function load () {
            OrderService.getOrder(orderId, init)
        };
        load();

        OrderService.getOrderStatusList(function(data) {
            $scope.orderStatusList = data;
        });

        OrderService.getActionStatusList(function(data) {
            $scope.actionStatusList = data;
        });

        OrderService.getActionTypeList(function(typeList) {
            $scope.actionTypeList = typeList;
        });

        $scope.getNoteAuthor = function (note) {
            return OrderService.getNoteAuthor(note, $scope.order);
        };

        $scope.addAction = function () {
            OrderService.addAction($scope.order);
        };

        $scope.addPart = function () {
            OrderService.addPart($scope.order);
        };

        $scope.addNote = function () {
            $scope.order.notes.push({ note: $scope.note });
            $scope.note = "";
            $scope.showNoteAdd = false;
        };

        $scope.deleteAction = function (index) {
            $scope.order.actions.splice(index, 1);
        };

        $scope.deletePart = function (index) {
            $scope.order.parts.splice(index, 1);
        };

        $scope.saveAndManageDevices = function () {
            $scope.saveAndManageDevicesMessage = "";
            $scope.updateOrderFields();
            OrderService.updateOrder($scope.order, function(data) {
                if (data.success) {
                    $location.path("/detail/" + orderId + "/servicedevice/");
                }
                else {
                    $scope.saveAndManageDevicesMessage = data.message || "Viga salvestamisel";
                    $scope.errors = data.errors;
                }
            });

        };

        $scope.saveChanges = function () {
            $scope.message = "";
            $scope.updateOrderFields();
            OrderService.updateOrder($scope.order, function(data) {
                if (data.success) {
                    load();
                    $scope.message = "Salvestatud";
                    $scope.errors = null;
                }
                else {
                    $scope.message = data.message || "Viga salvestamisel";
                    $scope.errors = data.errors;
                }
            });
        };

        $scope.updateOrderFields = function () {
            $scope.order.status = findByID($scope.orderStatusList, $scope.selectedOrderStatus);
            for (var i = 0; i < $scope.order.actions.length; i++) {
                var action = $scope.order.actions[i];
                action.status = findByID($scope.actionStatusList, action.status.id);
                OrderService.setDevice(action, $scope.order.serviceDevices);
            }
            for (var i = 0; i < $scope.order.parts.length; i++) {
                var part = $scope.order.parts[i];
                OrderService.setDevice(part, $scope.order.serviceDevices);
            }
        };

        $scope.actionTypeUpdated = function(action) {
            if (action.selectedTypeId !== action.type.id) {
                action.type = findByID($scope.actionTypeList, action.selectedTypeId);
                action.price = action.type.service_price;
            }
        };

        $scope.delete = function () {
            OrderService.deleteOrder($scope.order.id, function (data) {
                if (data.success) {
                    $location.path("/search");
                }
                else {
                    $scope.message = data.message || "Viga kustutamisel";
                    $scope.errors = data.errors;
                }
            })
        };

        $scope.openInvoiceForm = function () {
            if (!$scope.invoiceExists) {
                $scope.invoice.customer.id = $scope.order.service_request.customer_fk;
                $scope.invoice.order.id = $scope.order.id;
                $scope.invoice.description = $scope.order.note;
                $scope.invoice.receiver_name = $scope.order.customer_name;
            }
            $scope.invoiceFormOpen = !$scope.invoiceFormOpen;
        };

        $scope.saveInvoice = function () {
            $scope.invoice.due_date = Date.parse($scope.invoice.due_date);
            if ($scope.invoice.due_date === undefined || $scope.invoice.receiver_accounts === undefined) {
                $scope.message = "Nõutud väljad täitmata!";
            } else {
                InvoiceService.saveInvoice($scope.invoice, function (data) {
                    $scope.loadInvoice();
                })
            }
        };

        $scope.searchClient = function () {
            if ($scope.invoice.receiver_name !== undefined) {
                InvoiceService.getClients($scope.client_name, function (data) {
                    $scope.clients = data;
                })
            }
        };

        $scope.chooseClient = function (id, name) {
            $scope.invoice.customer.id = id;
            $scope.invoice.receiver_name = name;
        };

        $scope.loadInvoice = function () {
            if ($scope.orderStatusInServer == 3) {
                InvoiceService.getInvoice($scope.order.id, function (data) {
                    if (data) {
                        data.due_date = new Date(data.due_date);
                        $scope.invoice = data;
                        $scope.invoiceExists = true;
                    }
                })
            }
        };

        $scope.confirmInvoice = function () {
            if ($scope.invoice !== undefined) {
                InvoiceService.confirmInvoice($scope.invoice.invoice, function (data) {
                    $scope.loadInvoice();
                });
            }
        }

    });

detail.directive('actions', function () {
    return {templateUrl: 'order/actions.html'}
});
detail.directive('devices', function () {
    return {templateUrl: 'order/devices.html'}
});
detail.directive('notes', function () {
    return {templateUrl: 'order/notes.html'}
});
detail.directive('parts', function () {
    return {templateUrl: 'order/parts.html'}
});
detail.directive('invoice', function () {
    return {templateUrl: 'order/invoice.html'}
});
