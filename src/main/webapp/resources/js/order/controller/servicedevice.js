/**
 * Created by karmo on 2.06.15.
 */
angular.module("ServiceDevice", ['ngRoute'])

    .controller("ServiceDeviceController", function (
        $scope, $location, $routeParams, OrderService, ServiceDeviceService) {

        $scope.orderId = parseInt($routeParams.orderId);
        var serviceDeviceId = parseInt($routeParams.id);
        $scope.showAddDevice = false;

        var loadDeviceList = function () {
            ServiceDeviceService.getServiceDeviceList(
                $scope.orderId, function (data) {
                    $scope.list = data;
                    if (!serviceDeviceId && data && data.length > 0) {
                        $scope.selectedServiceDevice = data[0].id;
                        $scope.changeDevice();
                    }
                    else
                        $scope.selectedServiceDevice = serviceDeviceId;
                });
        };
        loadDeviceList();

        ServiceDeviceService.getStatusList(function (data) {
            $scope.statusList = data;
        });

        function load() {
            ServiceDeviceService.getServiceDevice(serviceDeviceId, function (data) {
                $scope.sd = data;
                ServiceDeviceService.setDates($scope.sd);
                $scope.order = {
                    actions: $scope.sd.actions,
                    parts: $scope.sd.parts
                };
                $scope.orderStatusInServer = 5; // This disables editing
                $scope.selectedStatusId = $scope.sd.status.id;
            })
        }
        if (serviceDeviceId)
            load();

        $scope.changeDevice = function () {
            $location.path("/detail/" + $scope.orderId
                + "/servicedevice/" + $scope.selectedServiceDevice);
        };

        $scope.statusUpdated = function () {
            $scope.sd.status = findByID($scope.statusList, $scope.selectedStatusId);
        };

        $scope.fromDateChanged = function () {
            $scope.sd.from_store = $scope.sd.fromDate.getTime();
        };

        $scope.toDateChanged = function () {
            $scope.sd.to_store = $scope.sd.toDate.getTime();
        };

        $scope.save = function () {
            ServiceDeviceService.update($scope.sd, function (data) {
                $scope.message = "";
                if (data.success) {
                    load();
                    $scope.message = "Salvestatud";
                    $scope.errors = null;
                }
                else {
                    $scope.message = data.message || "Viga salvestamisel";
                    $scope.errors = data.errors;
                }
            })
        };

        $scope.removeServiceDevice = function () {
            $scope.removeSDMessage = "";
            ServiceDeviceService.remove($scope.sd.id, function (data) {
                if (data.success) {
                    $location.path("/detail/" + $scope.orderId + "/servicedevice/");
                    $scope.errors = null;
                } else {
                    $scope.removeSDMessage = data.message || "Viga seadme eemaldamisel";
                    $scope.errors = data.errors;
                }
            });
        };

        $scope.showAddDevice = function () {
            showAddDeviceScreen();
            retreiveDeviceTypes();
            $(".adddevice").css("margin-top", "30px");
            $(".adddevice").css("margin-left", "0px");
            $scope.showAddDevice = true;
        };

        $scope.addDevices = function () {
            OrderService.addDevices($scope.orderId, devices, function (data) {
                if (data.success) {
                    loadDeviceList();
                    $scope.showAddDevice = false;
                    $scope.addDevicesMessage = "Lisamine õnnestus.";
                } else {
                    $scope.addDevicesMessage = data.message || "Viga seadmete lisamisel";
                }
            });
        };
    })

.directive('adddevice', function() { return { templateUrl: 'adddevice.html' } });
