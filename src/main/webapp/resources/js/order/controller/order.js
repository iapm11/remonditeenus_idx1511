/**
 * Created by karmo on 24.05.15.
 */
var app = angular.module("Order",
    ['ngRoute', 'SearchOrders', 'OrderDetail', 'ServiceDevice']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/detail/:orderId',
        {
            controller: 'DetailController',
            templateUrl: 'order/detail.html'
        })
        .when('/search',
        {
            controller: 'SearchOrdersController',
            templateUrl: 'order/search.html'
        })
        .when('/detail/:orderId/servicedevice/:id?',
        {
            controller: 'ServiceDeviceController',
            templateUrl: 'order/servicedevice.html'
        })
        .otherwise({ redirectTo: '/search' });
});

app.controller("OrderController", function($scope) {

});