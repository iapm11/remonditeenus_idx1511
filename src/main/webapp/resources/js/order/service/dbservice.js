/**
 * Created by karmo on 25.05.15.
 */
angular.module('Order')
.service('DBService', function ($http) {
        this.postData = function(url, params, callback) {
            $http({
                method: 'POST',
                url: url,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                },
                data: $.param(params)
            })
                .success(function (data) {
                    callback(data);
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                    console.log(status);
                    console.log(headers);
                    console.log(config);
                    callback(data);
                });
        };

        this.postAsJson = function (url, obj, callback) {
            $.ajax({
                url: url,
                context: document.body,
                method: "POST",
                headers: {'Content-Type': 'application/json'},
                data: JSON.stringify(obj)
            }).success(function (data) {
                callback(data);
            }).error(function(data) {
                callback(data);
            });
        };

        this.getData = function(url, callback) {
            $http({
                method: 'GET',
                url: url
            })
                .success(function (data) {
                    callback(data);
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
                    console.log(status);
                    console.log(headers);
                    console.log(config);
                });
        }
    });