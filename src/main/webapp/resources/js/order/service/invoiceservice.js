angular.module('Order')
    .service('InvoiceService', function (DBService) {

        this.saveInvoice = function (invoice, callback) {
            var url = "invoice/save/";
            DBService.postAsJson(url, invoice, callback);
        };

        this.getClients = function (name, callback) {
            var url = "client/findbyname/" + name;
            DBService.getData(url, callback);
        };

        this.getInvoice = function (orderID, callback) {
            var url = "invoice/" + orderID;
            DBService.getData(url, callback);
        };

        this.confirmInvoice = function (invoiceId, callback) {
            var url = "invoice/confirm/" + invoiceId;
            DBService.postData(url, '', callback);
        }
    });