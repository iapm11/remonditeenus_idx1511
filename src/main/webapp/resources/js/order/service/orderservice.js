/**
 * Created by karmo on 25.05.15.
 */
angular.module('Order')
    .service('OrderService', function (DBService) {

        this.searchOrder = function(query, callback) {
            var url = "order/searchorder";
            DBService.postAsJson(url, query, callback);
        };

        this.getOrder = function(orderId, callback) {
            var url = "order/id/" + orderId;
            DBService.getData(url, callback);
        };

        this.getActionStatusList = function(callback) {
            var url = "order/action/statuslist";
            DBService.getData(url, callback);
        };

        this.getActionTypeList = function(callback) {
            var url = "order/action/typelist";
            DBService.getData(url, callback);
        };

        this.getOrderStatusList = function(callback) {
            var url = "order/statuslist";
            DBService.getData(url, callback);
        };

        this.updateOrder = function(order, callback) {
            var url = "order/update";
            DBService.postData(url, {
                order: JSON.stringify(order)
            }, callback);
        };

        this.deleteOrder = function (orderId, callback) {
            var url = "order/delete";
            DBService.postData(url, {
                orderid: orderId
            }, callback);
        };

        this.addDevices = function (orderId, devices, callback) {
            if (devices.length == 0) {
                alert("Seadmeid ei ole valitud");
                return;
            }
            var url = "/rteenus/order/adddevices/" + orderId;
            DBService.postAsJson(url, devices, callback);
        };

        function getEmployeeName(emp) {
            if (emp)
                return emp.person.first_name + " " + emp.person.last_name;
            return "";
        };

        this.getNoteAuthor = function (note, order) {
            if (note.note_author_type == 1)
                return "Klient: " + order.customer_name;
            else if (note.note_author_type == 2)
                return "Töötaja: " + getEmployeeName(note.employee);
            return "";
        };

        function setDefaultDevice(obj, serviceDevices) {
            if (serviceDevices && serviceDevices.length > 0)
                obj.device = serviceDevices[0].device;
        };

        this.addAction = function (order) {
            var action = new Action();
            setDefaultDevice(action, order.serviceDevices);
            order.actions.push(action);
        };

        this.addPart = function (order) {
            var part = new Part();
            setDefaultDevice(part, order.serviceDevices);
            order.parts.push(part);
        };

        this.setDevice = function (obj, serviceDevices) {
            var sd = _.find(serviceDevices, function (serviceDevice) {
                return serviceDevice.device.device == obj.device.device;
            });
            if (sd)
                obj.device = sd.device;
        };

    });

function findByID(arr, id) {
    return _.findWhere(arr, {"id": id});
}
