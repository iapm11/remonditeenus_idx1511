/**
 * Created by karmo on 3.06.15.
 */
angular.module("ServiceDevice")
    .service('ServiceDeviceService', function (DBService) {

        this.getServiceDeviceList = function (orderId, callback) {
            var url = "servicedevice/orderid/" + orderId;
            DBService.getData(url, callback);
        };

        this.getServiceDevice = function (serviceDeviceId, callback) {
            var url = "servicedevice/id/" + serviceDeviceId;
            DBService.getData(url, callback);
        };

        this.getStatusList = function (callback) {
            var url = "servicedevice/statuslist/";
            DBService.getData(url, callback);
        };

        this.update = function (serviceDevice, callback) {
            var url = "servicedevice/update/";
            DBService.postData(url, {
                servicedevice: JSON.stringify(serviceDevice)
            }, callback);
        };

        this.remove = function (serviceDeviceId, callback) {
            var url = "servicedevice/remove/" + serviceDeviceId;
            DBService.postData(url, {
                id: serviceDeviceId
            }, callback);
        };

        this.setDates = function (sd) {
            sd.fromDate = new Date(sd.from_store);
            if (sd.to_store)
                sd.toDate = new Date(sd.to_store);
        };

    });