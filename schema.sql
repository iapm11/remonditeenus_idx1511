--lisame siia endatehtud vaated/funktsioonid/muu jura

CREATE OR REPLACE FUNCTION f_cancel_request
  (p_request_id service_request.service_request%TYPE)
  RETURNS VOID AS $$
UPDATE service_request
SET service_request_status_type_fk = 2
WHERE service_request = p_request_id;
$$ LANGUAGE SQL SECURITY DEFINER
SET search_path = PUBLIC, pg_temp;

CREATE OR REPLACE FUNCTION f_accept_request
  (p_request_id service_request.service_request%TYPE)
  RETURNS VOID AS $$
UPDATE service_request
SET service_request_status_type_fk = 3
WHERE service_request = p_request_id;
$$ LANGUAGE SQL SECURITY DEFINER
SET search_path = PUBLIC, pg_temp;




SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 't120941_r'
      AND pid <> pg_backend_pid();

CREATE OR REPLACE FUNCTION f_update_order_price_total()
  RETURNS trigger AS $$
    DECLARE
      id INTEGER;
BEGIN
  IF TG_OP = 'INSERT' THEN
    id := NEW.service_order_fk;
  ELSEIF TG_OP = 'UPDATE' THEN
    id := COALESCE(NEW.service_order_fk, OLD.service_order_fk);
  ELSEIF TG_OP = 'DELETE' THEN
    id := OLD.service_order_fk;
  END IF;

  UPDATE service_order so
  SET price_total = subquery.price_sum
  FROM
    (SELECT service_order,
       COALESCE((SELECT SUM(a.service_amount * a.price) AS action_sum
          FROM service_order so1
          INNER JOIN service_action a ON service_order = a.service_order_fk
          WHERE so1.service_order = so_sub.service_order
          GROUP BY service_order), 0)
     + COALESCE((SELECT SUM(p.part_count * p.part_price) AS part_sum
          FROM service_order so2
          INNER JOIN service_part p ON service_order = p.service_order_fk
          WHERE so2.service_order = so_sub.service_order
          GROUP BY service_order), 0) AS price_sum
     FROM service_order as so_sub
     WHERE service_order = id) AS subquery
  WHERE so.service_order = subquery.service_order;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER
SET search_path = public, pg_temp;

DROP TRIGGER IF EXISTS trig_action_update_order_price_total ON service_action;
CREATE TRIGGER trig_action_update_order_price_total
AFTER INSERT OR DELETE OR UPDATE ON service_action
FOR EACH ROW
EXECUTE PROCEDURE f_update_order_price_total();

DROP TRIGGER IF EXISTS trig_part_update_order_price_total ON service_part;
CREATE TRIGGER trig_part_update_order_price_total
AFTER INSERT OR DELETE OR UPDATE ON service_part
FOR EACH ROW
EXECUTE PROCEDURE f_update_order_price_total();


